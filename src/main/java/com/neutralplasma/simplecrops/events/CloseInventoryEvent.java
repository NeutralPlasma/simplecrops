package com.neutralplasma.simplecrops.events;


import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.gui.Handler;
import eu.virtusdevelops.virtuscore.gui.InventoryCreator;
import eu.virtusdevelops.virtuscore.gui.actions.InventoryCloseAction;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class CloseInventoryEvent implements Listener {

    private SimpleCrops simpleCrops;
    private Handler handler;

    public CloseInventoryEvent(SimpleCrops simpleCrops, Handler handler){
        this.handler = handler;
        this.simpleCrops = simpleCrops;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInventoryClose(InventoryCloseEvent event){
        // remove player
        handler.removeFromList(event.getPlayer().getUniqueId());

        //Get our CustomHolder
        InventoryCreator customHolder = null;
        try {
            customHolder = (InventoryCreator) event.getView().getTopInventory().getHolder();
        }catch (Exception error){

        }
        if(customHolder != null){
            for(InventoryCloseAction closeAction : customHolder.getCloseActions()){
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        Player player = (Player) event.getPlayer();
                        if(!handler.hasOpened(event.getPlayer().getUniqueId())) {
                            closeAction.execute((Player) event.getPlayer(), event.getInventory());
                        }
                    }
                }.runTaskLater(simpleCrops, 2L);
            }
        }
    }
}
