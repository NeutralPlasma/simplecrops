package com.neutralplasma.simplecrops.events.crop;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.*;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class CropPlaceEvent implements Listener {
    private SimpleCrops simpleCrops;
    private DataManager dataManager;
    private CropDrops cropDrops;
    private MessagesData messagesData;
    private MessagesHandler messagesHandler;
    private MultiCropHandler multiCropHandler;
    private NBTDataUtil dataUtil;

    private ArrayList<String> cropType;

    public CropPlaceEvent(SimpleCrops simpleCrops, DataManager dataManager, CropDrops cropDrops, MessagesData messagesData,
                          MessagesHandler messagesHandler, MultiCropHandler multiCropHandler, NBTDataUtil nbtDataUtil){
        this.simpleCrops = simpleCrops;
        this.dataManager = dataManager;
        this.cropDrops = cropDrops;
        this.messagesData = messagesData;
        this.messagesHandler = messagesHandler;
        this.multiCropHandler = multiCropHandler;
        this.dataUtil = nbtDataUtil;

        cropType = (ArrayList<String>) simpleCrops.getConfig().getStringList("crop-types");
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onCropPlace(BlockPlaceEvent event){
        Block placedBlock = event.getBlock();

        Location loc = placedBlock.getLocation();
        loc.setY(loc.getY() - 1);
        Block placedOn = loc.getBlock();

        // ANTI ILLEGAL MULTICROP STACKING
        if(multiCropHandler.placedOnCrop(placedOn)){
            if(multiCropHandler.isMultiCrop(multiCropHandler.getBaseBlock(placedOn))){
                Block baseBlock = multiCropHandler.getBaseBlock(placedOn);
                CropLocation cropLocation = new CropLocation(baseBlock.getWorld().toString(), baseBlock.getX(), baseBlock.getY(), baseBlock.getZ());
                Crop crop = dataManager.getCrop(cropLocation);
                if(crop != null){
                    Player player = event.getPlayer();
                    messagesHandler.debug(player, placedBlock, "ILLEGAL-PLACE");
                    if (!player.hasPermission("simplecrops.admin.bypass")) {
                        event.setCancelled(true);
                    }
                    return;
                }
            }
        }

        ItemStack item = event.getItemInHand();
        String itemname = item.getItemMeta().getDisplayName();
        if(item.hasItemMeta()) {
            Player player = event.getPlayer();
            if (cropType.contains(placedBlock.getType().toString())) {
                if (player.hasPermission("simplecrops.use")) {
                    if (saveCrop(placedBlock, player, item, itemname, dataUtil.getString(item, "id"))) {
                    } else {
                        event.setCancelled(true);
                        player.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("waitPlace")));
                        return;
                    }
                }else{
                    player.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("cantPlace")));
                    event.setCancelled(true);
                }
            }
        }

    }

    public boolean saveCrop(Block block, Player player, ItemStack item, String name, String cropID){
        int gain = dataUtil.getInt(item, "gain");
        int strength = dataUtil.getInt(item, "strength");
        Crop crop = new Crop(name, gain, strength, player.getUniqueId(), cropID);
        CropLocation cropLocation = new CropLocation(block.getLocation().getWorld().toString(), block.getX(), block.getY(), block.getZ());
        if(dataManager.addPlant(crop, cropLocation)){
            messagesHandler.debug(player, block, "place");
            return true;
        }
        return false;
    }
}
