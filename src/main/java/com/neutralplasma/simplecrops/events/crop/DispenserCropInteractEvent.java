package com.neutralplasma.simplecrops.events.crop;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Directional;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.inventory.ItemStack;

public class DispenserCropInteractEvent implements Listener {


    private SimpleCrops simpleCrops;
    private DataManager dataManager;
    private CropDrops cropDrops;
    private MessagesData messagesData;
    private MessagesHandler messagesHandler;
    private MultiCropHandler multiCropHandler;
    private NBTDataUtil nbtDataUtil;

    public DispenserCropInteractEvent(SimpleCrops simpleCrops, DataManager dataManager, CropDrops cropDrops, MessagesData messagesData,
                                      MessagesHandler messagesHandler, MultiCropHandler multiCropHandler, NBTDataUtil nbtDataUtil){
        this.simpleCrops = simpleCrops;
        this.dataManager = dataManager;
        this.cropDrops = cropDrops;
        this.messagesData = messagesData;
        this.messagesHandler = messagesHandler;
        this.multiCropHandler = multiCropHandler;
        this.nbtDataUtil = nbtDataUtil;

    }


    @EventHandler
    public void onDispenserInteract(BlockDispenseEvent event){
        Block block = event.getBlock();
        Directional data = (Directional) block.getBlockData();
        Block nblock = BlockUtil.getLocation(data.getFacing(), block);
        ItemStack item = event.getItem();
        if(item.getType() == Material.BONE_MEAL){
            if(multiCropHandler.isMultiCrop(multiCropHandler.getBaseBlock(nblock))){
                Block baseblock = multiCropHandler.getBaseBlock(nblock);
                CropLocation cropLocation = new CropLocation(baseblock.getWorld().toString(), baseblock.getX(), baseblock.getY(), baseblock.getZ());
                Crop crop = dataManager.getCrop(cropLocation);
                if(crop != null){
                    event.setCancelled(true);
                }
            }
            if(multiCropHandler.isCrop(nblock)){
                CropLocation cropLocation = new CropLocation(nblock.getWorld().toString(), nblock.getX(), nblock.getY(), nblock.getZ());
                Crop crop = dataManager.getCrop(cropLocation);
                if(crop != null){
                    if(!simpleCrops.getConfig().getBoolean("mehanics.dispenser_dispense")){
                        event.setCancelled(true);
                    }else{
                        event.setCancelled(true);
                        int bonemeal = crop.getBonemeal();
                        int maxBoneMeal = simpleCrops.getFileManager().getConfiguration("configuration/crops").getInt("crops." + crop.getid() + ".bonemeal.number");
                        dataManager.removeCrop(cropLocation, crop);
                        bonemeal = bonemeal + 1;
                        if (bonemeal >= maxBoneMeal) {
                            cropDrops.setGrowth(nblock, "third");
                        } else if (bonemeal > maxBoneMeal / 2) {
                            cropDrops.setGrowth(nblock, "second");
                        } else {
                            cropDrops.setGrowth(nblock, "first");
                        }
                        crop.setBonemeal(bonemeal);
                        dataManager.addPlant(crop, cropLocation);
                        item.setAmount(item.getAmount() - 1);
                    }
                }
            }
        }
    }
}
