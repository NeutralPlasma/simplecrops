package com.neutralplasma.simplecrops.events.crop;

import com.neutralplasma.simplecrops.SimpleCrops;
import eu.virtusdevelops.virtuscore.compatibility.ServerVersion;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;

import java.util.ArrayList;

public class MultiCropHandler {
    private SimpleCrops simpleCrops;

    public MultiCropHandler(SimpleCrops simpleCrops){
        this.simpleCrops = simpleCrops;
    }

    public Block getBaseBlock(Block block){
        Location blockLocation = block.getLocation();

        while(blockLocation.getBlock().getBlockData() instanceof Ageable){
            blockLocation.setY(blockLocation.getBlockY() - 1);
        }
        //blockLocation.setY(blockLocation.getBlockY() + 1);
        return blockLocation.getBlock();

        /*if (ServerVersion.isServerVersionAtOrBelow(ServerVersion.V1_13)){
            while (blockLocation.getBlock().getType() == Material.CACTUS
                    || blockLocation.getBlock().getType() == Material.SUGAR_CANE){
                blockLocation.setY(blockLocation.getBlockY() - 1);
            }
            blockLocation.setY(blockLocation.getBlockY() + 1);
            return blockLocation.getBlock();

        }else if(ServerVersion.isServerVersion(ServerVersion.V1_14)){
            while (blockLocation.getBlock().getType() == Material.CACTUS
                    || blockLocation.getBlock().getType() == Material.SUGAR_CANE
                    || blockLocation.getBlock().getType() == Material.BAMBOO
                    || blockLocation.getBlock().getType() == Material.BAMBOO_SAPLING
                    || blockLocation.getBlock().getType() == Material.KELP
                    || blockLocation.getBlock().getType() == Material.KELP_PLANT){
                blockLocation.setY(blockLocation.getBlockY() - 1);
            }
            blockLocation.setY(blockLocation.getBlockY() + 1);
            return blockLocation.getBlock();

        }else if(ServerVersion.isServerVersionAtLeast(ServerVersion.V1_15)){
            while (blockLocation.getBlock().getType() == Material.CACTUS
                    || blockLocation.getBlock().getType() == Material.SUGAR_CANE
                    || blockLocation.getBlock().getType() == Material.BAMBOO
                    || blockLocation.getBlock().getType() == Material.BAMBOO_SAPLING
                    || blockLocation.getBlock().getType() == Material.KELP
                    || blockLocation.getBlock().getType() == Material.KELP_PLANT){
                blockLocation.setY(blockLocation.getBlockY() - 1);
            }
            blockLocation.setY(blockLocation.getBlockY() + 1);
            return blockLocation.getBlock();
        }
        return blockLocation.getBlock();*/
    }


    public Block getBaseBlock(Block block, Material ignore, int limit){
        Location blockLocation = block.getLocation();
        int current = 0;
        while((blockLocation.getBlock() instanceof Ageable
                || blockLocation.getBlock().getType() == ignore )
                && limit >= current){
            blockLocation.setY(blockLocation.getBlockY() - 1);
            current++;
        }
        //blockLocation.setY(blockLocation.getBlockY() + 1);
        return blockLocation.getBlock();
        /*

        if (ServerVersion.isServerVersionAtOrBelow(ServerVersion.V1_13)){
            while (blockLocation.getBlock().getType() == Material.CACTUS
                    || blockLocation.getBlock().getType() == Material.SUGAR_CANE
                    || (limit >=  current && blockLocation.getBlock().getType() == ignore)){
                blockLocation.setY(blockLocation.getBlockY() - 1);
            }
            blockLocation.setY(blockLocation.getBlockY() + 1);
            return blockLocation.getBlock();

        }else if(ServerVersion.isServerVersion(ServerVersion.V1_14)){
            while (blockLocation.getBlock().getType() == Material.CACTUS
                    || blockLocation.getBlock().getType() == Material.SUGAR_CANE
                    || blockLocation.getBlock().getType() == Material.BAMBOO
                    || blockLocation.getBlock().getType() == Material.BAMBOO_SAPLING
                    || blockLocation.getBlock().getType() == Material.KELP
                    || blockLocation.getBlock().getType() == Material.KELP_PLANT
                    || (limit >=  current && blockLocation.getBlock().getType() == ignore)){
                blockLocation.setY(blockLocation.getBlockY() - 1);
            }
            blockLocation.setY(blockLocation.getBlockY() + 1);
            return blockLocation.getBlock();

        }else if(ServerVersion.isServerVersionAtLeast(ServerVersion.V1_15)){
            while (blockLocation.getBlock().getType() == Material.CACTUS
                    || blockLocation.getBlock().getType() == Material.SUGAR_CANE
                    || blockLocation.getBlock().getType() == Material.BAMBOO
                    || blockLocation.getBlock().getType() == Material.BAMBOO_SAPLING
                    || blockLocation.getBlock().getType() == Material.KELP
                    || blockLocation.getBlock().getType() == Material.KELP_PLANT
                    || (limit >=  current && blockLocation.getBlock().getType() == ignore)){
                blockLocation.setY(blockLocation.getBlockY() - 1);
            }
            blockLocation.setY(blockLocation.getBlockY() + 1);
            return blockLocation.getBlock();
        }
        return blockLocation.getBlock();*/
    }


    public boolean isMultiCrop(Block baseBlock){
        if(ServerVersion.isServerVersionAtOrBelow(ServerVersion.V1_13)){
            Material baseMaterial = baseBlock.getType();
            return baseMaterial == Material.CACTUS || baseMaterial == Material.SUGAR_CANE;
        }else if(ServerVersion.isServerVersion(ServerVersion.V1_14)){
            Material baseMaterial = baseBlock.getType();
            return baseMaterial == Material.CACTUS
                    || baseMaterial == Material.SUGAR_CANE
                    || baseMaterial == Material.BAMBOO
                    || baseMaterial == Material.BAMBOO_SAPLING
                    || baseMaterial == Material.KELP
                    || baseMaterial == Material.KELP_PLANT;

        }else if(ServerVersion.isServerVersionAtLeast(ServerVersion.V1_15)){
            Material baseMaterial = baseBlock.getType();
            return baseMaterial == Material.CACTUS
                    || baseMaterial == Material.SUGAR_CANE
                    || baseMaterial == Material.BAMBOO
                    || baseMaterial == Material.BAMBOO_SAPLING
                    || baseMaterial == Material.KELP
                    || baseMaterial == Material.KELP_PLANT;
        }
        return false;
    }

    public boolean placedOnCrop(Block block){

        if(block.getBlockData() instanceof Ageable){
            return true;
        }

        /*if (ServerVersion.isServerVersionAtOrBelow(ServerVersion.V1_13)){
            return block.getType() == Material.CACTUS || block.getType() == Material.SUGAR_CANE;
        }else if (ServerVersion.isServerVersion(ServerVersion.V1_14)){
            return block.getType() == Material.CACTUS
                    || block.getType() == Material.SUGAR_CANE
                    || block.getType() == Material.BAMBOO
                    || block.getType() == Material.BAMBOO_SAPLING
                    || block.getType() == Material.KELP
                    || block.getType() == Material.KELP_PLANT;
        }else if (ServerVersion.isServerVersionAtLeast(ServerVersion.V1_15)){
            return block.getType() == Material.CACTUS
                    || block.getType() == Material.SUGAR_CANE
                    || block.getType() == Material.BAMBOO
                    || block.getType() == Material.BAMBOO_SAPLING
                    || block.getType() == Material.KELP
                    || block.getType() == Material.KELP_PLANT;
        }*/
        return false;
    }

    public ArrayList<Block> getCropBlocks(Block baseBlock){
        ArrayList<Block> blocks = new ArrayList<>();
        Location blockLocation = baseBlock.getLocation();

        while (blockLocation.getBlock().getBlockData() instanceof Ageable){
            blocks.add(blockLocation.getBlock());
            blockLocation.setY(blockLocation.getBlockY() + 1);
        }

        /*if (ServerVersion.isServerVersionAtOrBelow(ServerVersion.V1_13)){
            while (blockLocation.getBlock().getType() == Material.CACTUS
                    || blockLocation.getBlock().getType() == Material.SUGAR_CANE){
                    blocks.add(blockLocation.getBlock());
                    blockLocation.setY(blockLocation.getBlockY() + 1);
            }
            return blocks;

        }else if(ServerVersion.isServerVersion(ServerVersion.V1_14)){
            while (blockLocation.getBlock().getType() == Material.CACTUS
                    || blockLocation.getBlock().getType() == Material.SUGAR_CANE
                    || blockLocation.getBlock().getType() == Material.BAMBOO
                    || blockLocation.getBlock().getType() == Material.BAMBOO_SAPLING
                    || blockLocation.getBlock().getType() == Material.KELP
                    || blockLocation.getBlock().getType() == Material.KELP_PLANT){
                blocks.add(blockLocation.getBlock());
                blockLocation.setY(blockLocation.getBlockY() + 1);
            }
            return blocks;

        }else if(ServerVersion.isServerVersionAtLeast(ServerVersion.V1_15)){
            while (blockLocation.getBlock().getType() == Material.CACTUS
                    || blockLocation.getBlock().getType() == Material.SUGAR_CANE
                    || blockLocation.getBlock().getType() == Material.BAMBOO
                    || blockLocation.getBlock().getType() == Material.BAMBOO_SAPLING
                    || blockLocation.getBlock().getType() == Material.KELP
                    || blockLocation.getBlock().getType() == Material.KELP_PLANT){
                blocks.add(blockLocation.getBlock());
                blockLocation.setY(blockLocation.getBlockY() + 1);
            }
            return blocks;
        }*/
        return blocks;
    }

    public boolean isCrop(Block block){
        //ArrayList<String> cropList = (ArrayList<String>) simpleCrops.getConfig().getList("crop-types");
        if(block.getBlockData() instanceof Ageable){
            return true;
        }
        /*for(String dd : cropList){
            if (dd.equalsIgnoreCase(block.getType().toString())){
                return true;
            }
        }*/
        return false;
    }
}
