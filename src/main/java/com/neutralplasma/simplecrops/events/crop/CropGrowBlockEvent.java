package com.neutralplasma.simplecrops.events.crop;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.CropDrops;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.world.StructureGrowEvent;

import java.util.ArrayList;

public class CropGrowBlockEvent implements Listener {
    private SimpleCrops simpleCrops;
    private DataManager dataManager;
    private CropDrops cropDrops;
    private MessagesData messagesData;
    private MessagesHandler messagesHandler;
    private MultiCropHandler multiCropHandler;


    public CropGrowBlockEvent(SimpleCrops simpleCrops, DataManager dataManager, CropDrops cropDrops, MessagesData messagesData,
                         MessagesHandler messagesHandler, MultiCropHandler multiCropHandler){
        this.simpleCrops = simpleCrops;
        this.dataManager = dataManager;
        this.cropDrops = cropDrops;
        this.messagesData = messagesData;
        this.messagesHandler = messagesHandler;
        this.multiCropHandler = multiCropHandler;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onCropGrow(StructureGrowEvent event) {

    }
}
