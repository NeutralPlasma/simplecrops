package com.neutralplasma.simplecrops.events.crop;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Directional;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockGrowEvent;

import java.util.ArrayList;

public class CropGrowEvent implements Listener {
    private SimpleCrops simpleCrops;
    private DataManager dataManager;
    private CropDrops cropDrops;
    private MessagesData messagesData;
    private MessagesHandler messagesHandler;
    private MultiCropHandler multiCropHandler;
    private ArrayList<String> breakBlocks = new ArrayList<>();


    public CropGrowEvent(SimpleCrops simpleCrops, DataManager dataManager, CropDrops cropDrops, MessagesData messagesData,
                         MessagesHandler messagesHandler, MultiCropHandler multiCropHandler){
        this.simpleCrops = simpleCrops;
        this.dataManager = dataManager;
        this.cropDrops = cropDrops;
        this.messagesData = messagesData;
        this.messagesHandler = messagesHandler;
        this.multiCropHandler = multiCropHandler;
        breakBlocks.add("AIR");
        breakBlocks.add("WATER");
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onCropGrow(BlockGrowEvent event){
        Block block = event.getBlock();
        if(block.getType() == Material.AIR){
            Block baseBlock = multiCropHandler.getBaseBlock(block, Material.AIR, 3);
            if(multiCropHandler.isMultiCrop(baseBlock)){
                if(baseBlock.getType() == Material.CACTUS) {
                    if (DoesGetBroken(block)) {
                        CropLocation cropLocation = new CropLocation(baseBlock.getWorld().toString(), baseBlock.getX(), baseBlock.getY(), baseBlock.getZ());
                        Crop crop = dataManager.getCrop(cropLocation);
                        if (crop != null) {
                            Player player = Bukkit.getPlayer(crop.getPlacedBy());
                            event.setCancelled(true);
                            cropDrops.dropDrops(block, crop, player);
                            messagesHandler.debug(player, block, "cropdrops");
                        }
                    }
                }
            }else{
                Bukkit.getScheduler().runTaskLater(simpleCrops, task -> {
                    if(block.getType() == Material.PUMPKIN){
                        Block block1 =  BlockUtil.getStemBlock(block, Material.ATTACHED_MELON_STEM);
                        Block block2 =  BlockUtil.getStemBlock(block, Material.ATTACHED_PUMPKIN_STEM);
                        Block stem = null;
                        if(block1 != null){
                            stem = block1;

                        }else if(block2 != null){
                            stem = block2;
                        }
                        if(stem != null){
                            CropLocation cropLocation = new CropLocation(stem.getWorld().toString(), stem.getX(), stem.getY(), stem.getZ());
                            Crop crop = dataManager.getCrop(cropLocation);
                            if(crop != null) {
                                //event.getNewState().getBlock().setType(Material.DIAMOND_BLOCK);
                                Player player = Bukkit.getPlayer(crop.getPlacedBy());
                                cropDrops.growBlocks(crop, event.getNewState().getBlock(), player);
                                messagesHandler.debug(player, block, "CROP_GROW");
                            }
                        }

                    }
                }, 1L);
            }
        }

    }

    public boolean DoesGetBroken(Block placed){
        World world = placed.getWorld();
        Block northOfPlaced = world.getBlockAt(placed.getLocation().clone().add(1,0,0));
        Block eastOfPlaced = world.getBlockAt(placed.getLocation().clone().add(0,0,1));
        Block southOfPlaced = world.getBlockAt(placed.getLocation().clone().add(-1,0,0));
        Block westOfPlaced = world.getBlockAt(placed.getLocation().clone().add(0,0,-1));
        if(!breakBlocks.contains(northOfPlaced.getType().toString())){
            return true;
        }
        if(!breakBlocks.contains(eastOfPlaced.getType().toString())){
            return true;
        }
        if(!breakBlocks.contains(southOfPlaced.getType().toString())){
            return true;
        }
        if(!breakBlocks.contains(westOfPlaced.getType().toString())){
            return true;
        }
        return false;
    }
}
