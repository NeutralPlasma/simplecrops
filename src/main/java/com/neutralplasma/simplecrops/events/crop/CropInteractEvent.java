package com.neutralplasma.simplecrops.events.crop;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.handlers.CropLeveling;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.*;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

public class CropInteractEvent implements Listener {
    private SimpleCrops simpleCrops;
    private DataManager dataManager;
    private CropDrops cropDrops;
    private MessagesData messagesData;
    private MessagesHandler messagesHandler;
    private MultiCropHandler multiCropHandler;
    private NBTDataUtil nbtDataUtil;
    private CropLeveling cropLeveling;

    private HashMap<UUID, Long> cooldowns = new HashMap<>();
    private static final Random random = new Random();

    public CropInteractEvent(SimpleCrops simpleCrops, DataManager dataManager, CropDrops cropDrops, MessagesData messagesData,
                             MessagesHandler messagesHandler, MultiCropHandler multiCropHandler, NBTDataUtil nbtDataUtil,
                             CropLeveling cropLeveling){
        this.simpleCrops = simpleCrops;
        this.dataManager = dataManager;
        this.cropDrops = cropDrops;
        this.messagesData = messagesData;
        this.messagesHandler = messagesHandler;
        this.multiCropHandler = multiCropHandler;
        this.nbtDataUtil = nbtDataUtil;
        this.cropLeveling = cropLeveling;


    }
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInteract(PlayerInteractEvent event) {
        Block block = event.getClickedBlock();
        Player player = event.getPlayer();
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && block != null) {
            if (multiCropHandler.isCrop(block)) {
                if (multiCropHandler.isMultiCrop(multiCropHandler.getBaseBlock(block))) {
                    Block baseblock = multiCropHandler.getBaseBlock(block);
                    CropLocation cropLocation = new CropLocation(baseblock.getWorld().toString(), baseblock.getX(), baseblock.getY(), baseblock.getZ());
                    Crop crop = dataManager.getCrop(cropLocation);
                    if (crop != null) {
                        ItemStack item = player.getInventory().getItemInMainHand();
                        if(item.getType() == Material.BONE_MEAL){
                            event.setCancelled(true);
                        }else{
                            item = player.getInventory().getItemInOffHand();
                            if(item.getType() == Material.BONE_MEAL){
                                event.setCancelled(true);
                            }
                        }
                        //event.setCancelled(true);
                        return;
                    }
                }
                ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
                CropLocation cropLocation = new CropLocation(block.getWorld().toString(), block.getX(), block.getY(), block.getZ());
                Crop crop = dataManager.getCrop(cropLocation);
                if (crop != null) {
                    if (block.getType().name().equalsIgnoreCase("SWEET_BERRY_BUSH"))
                        if(cropDrops.isFullyGrown(block))
                            event.setUseInteractedBlock(Event.Result.DENY);
                    if (item.getType() == Material.BONE_MEAL) {
                        if (simpleCrops.getFileManager().getConfiguration("configuration/crops").getBoolean("crops." + crop.getid() + ".bonemeal.useCustom")) {
                            event.setUseInteractedBlock(Event.Result.DENY);
                            boneMealHandler(cropLocation, crop, event.getPlayer(), item, block);
                        }
                    } else if (player.getInventory().getItemInOffHand().getType() == Material.BONE_MEAL) {
                        ItemStack nitem = player.getInventory().getItemInOffHand();
                        if (simpleCrops.getFileManager().getConfiguration("configuration/crops").getBoolean("crops." + crop.getid() + ".bonemeal.useCustom")) {
                            event.setUseInteractedBlock(Event.Result.DENY);
                            boneMealHandler(cropLocation, crop, event.getPlayer(), nitem, block);
                        }
                    } else if (item.getType().name().contains("HOE")) {
                        if (block.getType().name().equalsIgnoreCase("SWEET_BERRY_BUSH"))
                            event.setCancelled(true);

                        if (event.getPlayer().hasPermission("simplecrops.fastharvest")) {
                            long cooldown = simpleCrops.getConfig().getLong("hoe.cooldown");
                            long time = System.currentTimeMillis() - (cooldowns.get(player.getUniqueId()) == null ? cooldown : cooldowns.get(player.getUniqueId()));
                            if (time < cooldown) {
                                player.sendMessage(messagesHandler.getMessage("cooldown").replace("{time}", String.valueOf((cooldown - time) / 1000)));
                                return;
                            }
                            cooldowns.put(player.getUniqueId(), System.currentTimeMillis());
                            if (nbtDataUtil.getString(item, "id").equalsIgnoreCase("none")) {
                                if (cropDrops.isFullyGrown(block)) {
                                    if (dataManager.removeCrop(cropLocation, crop)) {
                                        crop.setBonemeal(0);
                                        LevelUpData data = cropLeveling.levelUpCrop(crop, "DD");
                                        if (data.getCrop() != null) {
                                            crop = data.getCrop();
                                        }
                                        if (data.didLevelUP()) {
                                            player.getWorld().playSound(block.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 15);
                                        }
                                        if (dataManager.addPlant(crop, cropLocation)) {
                                            cropDrops.dropDrops(block, crop, event.getPlayer());
                                            cropDrops.setGrowth(block, "first");
                                        }
                                    }
                                }
                            } else {
                                int radius = (nbtDataUtil.getInt(item, "HarvestSize") / 2);
                                int originalsize = nbtDataUtil.getInt(item, "HarvestSize");
                                int uses = nbtDataUtil.getInt(item, "Uses");
                                if (uses < 1) {
                                    player.sendMessage(messagesHandler.getMessage("hoe.noUsesLeft"));
                                    return;
                                }
                                boolean changedBlock = false;
                                for (Block b : BlockUtil.getSquare(multiCropHandler.getBaseBlock(block), radius)) {
                                    if (multiCropHandler.isCrop(b) && !multiCropHandler.isMultiCrop(multiCropHandler.getBaseBlock(b))) {
                                        if (cropDrops.isFullyGrown(b)) {
                                            CropLocation nCropLocation = new CropLocation(b.getWorld().toString(), b.getX(), b.getY(), b.getZ());
                                            Crop nCrop = dataManager.getCrop(nCropLocation);
                                            changedBlock = true;
                                            if (nCrop != null) {
                                                if (dataManager.removeCrop(nCropLocation, nCrop)) {
                                                    nCrop.setBonemeal(0);
                                                    LevelUpData data = cropLeveling.levelUpCrop(nCrop, "DD");
                                                    if (data.getCrop() != null) {
                                                        nCrop = data.getCrop();
                                                    }
                                                    if (data.didLevelUP()) {
                                                        player.getWorld().playSound(b.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 15);
                                                    }
                                                    if (dataManager.addPlant(nCrop, nCropLocation)) {
                                                        cropDrops.dropDrops(b, nCrop, player);
                                                        cropDrops.setGrowth(b, "first");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (changedBlock) {
                                    uses--;
                                    ItemMeta meta = nbtDataUtil.setInt(item, "Uses", uses);
                                    meta.setLore(messagesHandler.formatList(
                                            simpleCrops.getFileManager().getConfiguration("configuration/hoes").getStringList("hoes." + nbtDataUtil.getString(item, "id") + ".lore"),
                                            new String[]{"{uses}", "{size}"}, new String[]{String.valueOf(uses), String.valueOf(originalsize)}
                                    ));
                                    item.setItemMeta(meta);
                                    player.getInventory().setItemInMainHand(item);
                                    return;
                                }
                            }
                        } else if (block.getType().name().equalsIgnoreCase("SWEET_BERRY_BUSH")) {
                            if (dataManager.removeCrop(cropLocation, crop)) {
                                crop.setBonemeal(0);
                                LevelUpData data = cropLeveling.levelUpCrop(crop, "DD");
                                if (data.getCrop() != null) {
                                    crop = data.getCrop();
                                }
                                if (data.didLevelUP()) {
                                    player.getWorld().playSound(block.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 15);
                                }
                                if (dataManager.addPlant(crop, cropLocation)) {
                                    cropDrops.dropDrops(block, crop, player);
                                    cropDrops.setGrowth(block, "first");
                                }
                            }
                        }
                    }
                } else if (block.getType().name().equalsIgnoreCase("SWEET_BERRY_BUSH")) {
                    event.setCancelled(true);
                }
            }
        } else if (block.getType() == Material.DIRT || block.getType() == Material.GRASS_BLOCK) {
        }
        ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
        if (item == null) {
            return;
        }
        if (!player.hasPermission("simplecrops.prepareland")) {
            return;
        }
        if (item.getType() == Material.AIR) {
            return;
        }
        if (item.getType().toString().contains("HOE")) {
            if (!nbtDataUtil.getString(item, "id").equalsIgnoreCase("none")) {
                long cooldown = simpleCrops.getConfig().getLong("hoe.cooldown");
                long time = System.currentTimeMillis() - (cooldowns.get(player.getUniqueId()) == null ? cooldown : cooldowns.get(player.getUniqueId()));
                if (time < cooldown) {
                    player.sendMessage(messagesHandler.getMessage("cooldown").replace("{time}", String.valueOf((cooldown - time) / 1000)));
                    return;
                }

                int radius = (nbtDataUtil.getInt(item, "HarvestSize") / 2);
                int originalsize = nbtDataUtil.getInt(item, "HarvestSize");
                int uses = nbtDataUtil.getInt(item, "Uses");
                if (radius > 0) {
                    if (uses < 1) {
                        player.sendMessage(messagesHandler.getMessage("hoe.noUsesLeft"));
                        return;
                    }
                    boolean changedBlock = false;
                    for (Block b : BlockUtil.getSquare(multiCropHandler.getBaseBlock(block), radius)) {
                        if (b.getRelative(BlockFace.UP).getType() == Material.GRASS || b.getRelative(BlockFace.UP).getType() == Material.TALL_GRASS) {
                            b.getRelative(BlockFace.UP).breakNaturally();
                        }

                        if (b.getRelative(BlockFace.UP).getType() == Material.AIR && (b.getType() == Material.GRASS_BLOCK || b.getType() == Material.DIRT)) {
                            changedBlock = true;
                            Bukkit.getScheduler().runTaskLater(simpleCrops, () -> {
                                b.setType(Material.FARMLAND);
                                b.getWorld().playSound(b.getLocation(), Sound.BLOCK_GRASS_BREAK, 10, 15);
                            }, random.nextInt(15) + 1);

                        }
                    }
                    if (changedBlock) {
                        uses--;
                        ItemMeta meta = nbtDataUtil.setInt(item, "Uses", uses);
                        meta.setLore(messagesHandler.formatList(
                                simpleCrops.getFileManager().getConfiguration("configuration/hoes").getStringList("hoes." + nbtDataUtil.getString(item, "id") + ".lore"),
                                new String[]{"{uses}", "{size}"}, new String[]{String.valueOf(uses), String.valueOf(originalsize)}
                        ));
                        item.setItemMeta(meta);
                        player.getInventory().setItemInMainHand(item);
                        return;
                    }
                }
            }


        } else if (event.getAction().equals(Action.PHYSICAL)) {
            Location loc = block.getLocation();
            loc.setY(loc.getY() + 1);
            if (multiCropHandler.isCrop(loc.getBlock())) {
                if (!simpleCrops.getConfig().getBoolean("mehanics.interact")) {
                    event.setCancelled(true);
                } else {
                    Block blockabove = loc.getBlock();
                    CropLocation cropLocation = new CropLocation(blockabove.getLocation().getWorld().toString(), blockabove.getX(), blockabove.getY(), blockabove.getZ()); // Gets the crop location
                    Crop crop = dataManager.getCrop(cropLocation); // Gets the crop from database if its saved.
                    if (crop != null) {
                        if (player.hasPermission("simplecrops.use")) {
                            cropDrops.dropSeed(blockabove, crop, player, cropDrops.isFullyGrown(blockabove)); // drops new custom seed
                            cropDrops.dropDrops(blockabove, crop, player); // drops crop drops.
                            dataManager.removeCrop(cropLocation, crop); // removes crop from data
                            messagesHandler.debug(player, cropLocation, crop, "physical");
                            blockabove.setType(Material.AIR); // removes crop.
                        } else {
                            player.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("cantBreak")));
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInteract(EntityInteractEvent event) {
        Block block = event.getBlock();
        Location loc = block.getLocation();
        loc.setY(loc.getY() + 1);
        if (multiCropHandler.isCrop(loc.getBlock())) {
            if (!simpleCrops.getConfig().getBoolean("mehanics.interact")) {
                event.setCancelled(true);
            } else {
                Block blockabove = loc.getBlock();
                CropLocation cropLocation = new CropLocation(blockabove.getLocation().getWorld().toString(), blockabove.getX(), blockabove.getY(), blockabove.getZ()); // Gets the crop location
                Crop crop = dataManager.getCrop(cropLocation); // Gets the crop from database if its saved.
                if (crop != null) {
                    Player player = Bukkit.getPlayer(crop.getPlacedBy());
                    cropDrops.dropSeed(blockabove, crop, player, cropDrops.isFullyGrown(blockabove)); // drops new custom seed
                    cropDrops.dropDrops(blockabove, crop, player); // drops crop drops.
                    dataManager.removeCrop(cropLocation, crop); // removes crop from data
                    messagesHandler.debug(player, cropLocation, crop, "physical");
                    blockabove.setType(Material.AIR); // removes crop.
                }

            }
        }
    }

    public boolean boneMealHandler(CropLocation cropLocation, Crop crop, Player player, ItemStack itemInUse, Block block) {
        Material mat;
        if (itemInUse == null) {
            mat = Material.AIR;
        } else {
            mat = itemInUse.getType();
        }

        if (mat != Material.BONE_MEAL || cropDrops.isFullyGrown(block)) {
            return false;
        } else {
            int bonemeal = crop.getBonemeal();
            int maxBoneMeal = simpleCrops.getFileManager().getConfiguration("configuration/crops").getInt("crops." + crop.getid() + ".bonemeal.number");
            dataManager.removeCrop(cropLocation, crop);
            bonemeal = bonemeal + 1;
            if (bonemeal >= maxBoneMeal) {
                cropDrops.setGrowth(block, "third");
            } else if (bonemeal > maxBoneMeal / 2) {
                cropDrops.setGrowth(block, "second");
            } else {
                cropDrops.setGrowth(block, "first");
            }
            crop.setBonemeal(bonemeal);
            dataManager.addPlant(crop, cropLocation);
            block.getWorld().playEffect(block.getLocation(), Effect.VILLAGER_PLANT_GROW, 1);
            messagesHandler.debug(player, cropLocation, crop, "bonemeal");
            if (player.getGameMode() != GameMode.CREATIVE) {
                itemInUse.setAmount(itemInUse.getAmount() - 1);
            }
            return true;
        }
    }
}
