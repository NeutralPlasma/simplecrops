package com.neutralplasma.simplecrops.events.crop;

import com.neutralplasma.simplecrops.utils.Crop;

public class LevelUpData {
    Crop crop;
    boolean didLevelUP;

    public LevelUpData(Crop crop, boolean levelup){
        this.crop = crop;
        this.didLevelUP = levelup;
    }

    public boolean didLevelUP() {
        return didLevelUP;
    }
    public Crop getCrop() {
        return crop;
    }
}
