package com.neutralplasma.simplecrops.events.crop;

import com.mojang.datafixers.kinds.IdF;
import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.Crop;
import com.neutralplasma.simplecrops.utils.CropDrops;
import com.neutralplasma.simplecrops.utils.CropLocation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.ArrayList;

public class CropFromToEvent implements Listener {
    private SimpleCrops simpleCrops;
    private DataManager dataManager;
    private CropDrops cropDrops;
    private MessagesData messagesData;
    private MessagesHandler messagesHandler;
    private MultiCropHandler multiCropHandler;

    public CropFromToEvent(SimpleCrops simpleCrops, DataManager dataManager, CropDrops cropDrops, MessagesData messagesData,
                           MessagesHandler messagesHandler, MultiCropHandler multiCropHandler){
        this.simpleCrops = simpleCrops;
        this.dataManager = dataManager;
        this.cropDrops = cropDrops;
        this.messagesData = messagesData;
        this.messagesHandler = messagesHandler;
        this.multiCropHandler = multiCropHandler;
    }


    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockFromToEvent(org.bukkit.event.block.BlockFromToEvent event){
        Block block = event.getToBlock();
        if(multiCropHandler.isCrop(block)){
            if(multiCropHandler.isMultiCrop(multiCropHandler.getBaseBlock(block))){
                Block baseBlock = multiCropHandler.getBaseBlock(block);
                CropLocation cropLocation = new CropLocation(baseBlock.getWorld().toString(), baseBlock.getX(), baseBlock.getY(), baseBlock.getZ());
                Crop crop = dataManager.getCrop(cropLocation);
                if(crop != null){
                    if(!simpleCrops.getConfig().getBoolean("mehanics.water-flow")){
                        event.setCancelled(true);
                        return;
                    }
                    String currentType = baseBlock.getType().toString();
                    ArrayList<Block> blocks = multiCropHandler.getCropBlocks(block);
                    Player player = Bukkit.getPlayer(crop.getPlacedBy());
                    for (Block cblock : blocks) {
                        if(cblock.getY() != baseBlock.getY()) {
                            cropDrops.dropDrops(cblock, crop, player);
                            messagesHandler.debug(player, cblock, "cropdrops");
                        }
                        cblock.setType(Material.AIR);
                    }
                    if(!currentType.equalsIgnoreCase(baseBlock.getType().toString())){
                        cropDrops.dropSeed(baseBlock, crop, player, false);
                        messagesHandler.debug(player, baseBlock, "seeddrop");
                        dataManager.removeCrop(cropLocation, crop);
                    }
                }
            }else{
                CropLocation cropLocation = new CropLocation(block.getWorld().toString(), block.getX(), block.getY(), block.getZ());
                Crop crop = dataManager.getCrop(cropLocation);
                if(crop != null) {
                    if(!simpleCrops.getConfig().getBoolean("mehanics.water-flow")){
                        event.setCancelled(true);
                        return;
                    }
                    Player player = Bukkit.getPlayer(crop.getPlacedBy());
                    cropDrops.dropDrops(block, crop, player);
                    cropDrops.dropSeed(block, crop, player, cropDrops.isFullyGrown(block));
                    messagesHandler.debug(player, block, "seeddrop");
                    messagesHandler.debug(player, block, "cropdrops");

                    block.setType(Material.AIR);

                    dataManager.removeCrop(cropLocation, crop);
                }
            }

        }

    }
}
