package com.neutralplasma.simplecrops.events.crop;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.*;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.ArrayList;

public class CropBreakEvent implements Listener {
    private SimpleCrops simpleCrops;
    private CropDrops cropDrops;
    private ItemUtil itemUtil;
    private DataManager dataManager;
    private MessagesData messagesData;
    private MessagesHandler messagesHandler;
    private MultiCropHandler multiCropHandler;

    public CropBreakEvent(SimpleCrops simpleCrops ,CropDrops cropDrops, ItemUtil itemUtil, DataManager dataManager,
                          MessagesData messagesData, MessagesHandler messagesHandler, MultiCropHandler multiCropHandler){
        this.simpleCrops = simpleCrops;
        this.cropDrops = cropDrops;
        this.itemUtil = itemUtil;
        this.dataManager = dataManager;
        this.messagesData = messagesData;
        this.messagesHandler = messagesHandler;
        this.multiCropHandler = multiCropHandler;

    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onCropBreak(BlockBreakEvent event){
        Block block = event.getBlock();

        if(block.getType() == Material.DIRT
                || block.getType() == Material.SAND
                || block.getType() == Material.FARMLAND
                || block.getType() == Material.GRASS_BLOCK
                || block.getType() == Material.SOUL_SAND){
            Location bloc = block.getLocation();
            bloc.setY(bloc.getY() + 1);
            block = bloc.getBlock();
        }

        if(multiCropHandler.isMultiCrop(multiCropHandler.getBaseBlock(block))){
            Block baseBlock = multiCropHandler.getBaseBlock(block);
            CropLocation cropLocation = new CropLocation(baseBlock.getWorld().toString(), baseBlock.getX(), baseBlock.getY(), baseBlock.getZ());
            Crop crop = dataManager.getCrop(cropLocation);
            if(crop != null){
                event.setCancelled(true);
                String currentType = baseBlock.getType().toString();
                ArrayList<Block> blocks = multiCropHandler.getCropBlocks(block);
                Player player = Bukkit.getPlayer(crop.getPlacedBy());
                if(player == null){
                    player = event.getPlayer();
                }
                for (Block cblock : blocks) {
                    if(cblock.getY() != baseBlock.getY()) {
                        cropDrops.dropDrops(cblock, crop, player);
                        messagesHandler.debug(player, cblock, "cropdrops");
                    }
                    cblock.setType(Material.AIR);
                }
                if(!currentType.equalsIgnoreCase(baseBlock.getType().toString())){
                    cropDrops.dropSeed(baseBlock, crop, player, false);
                    messagesHandler.debug(player, baseBlock, "seeddrop");
                    if(!dataManager.removeCrop(cropLocation, crop)){
                        baseBlock.setType(Material.getMaterial(currentType));
                        player.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("waitPlace")));
                    }
                }
            }
        }else{
            CropLocation cropLocation = new CropLocation(block.getWorld().toString(), block.getX(), block.getY(), block.getZ());
            Crop crop = dataManager.getCrop(cropLocation);
            if(crop != null) {
                event.setCancelled(true);
                Player player = Bukkit.getPlayer(crop.getPlacedBy());
                if(player == null){
                    player = event.getPlayer();
                }
                if(dataManager.removeCrop(cropLocation, crop)){
                    cropDrops.dropDrops(block, crop, player);
                    cropDrops.dropSeed(block, crop, player, cropDrops.isFullyGrown(block));
                    messagesHandler.debug(player, block, "seeddrop");
                    messagesHandler.debug(player, block, "cropdrops");
                    block.setType(Material.AIR);
                }else{
                    player.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("waitPlace")));
                }
            }
        }
    }
}
