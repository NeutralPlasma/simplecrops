package com.neutralplasma.simplecrops.events.crop;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.BlockUtil;
import com.neutralplasma.simplecrops.utils.Crop;
import com.neutralplasma.simplecrops.utils.CropDrops;
import com.neutralplasma.simplecrops.utils.CropLocation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonRetractEvent;

import java.util.ArrayList;
import java.util.List;

public class CropPistonRetractEvent implements Listener {
    private SimpleCrops simpleCrops;
    private DataManager dataManager;
    private CropDrops cropDrops;
    private MessagesData messagesData;
    private MessagesHandler messagesHandler;
    private MultiCropHandler multiCropHandler;

    public CropPistonRetractEvent(SimpleCrops simpleCrops, DataManager dataManager, CropDrops cropDrops, MessagesData messagesData,
                                 MessagesHandler messagesHandler, MultiCropHandler multiCropHandler){
        this.simpleCrops = simpleCrops;
        this.dataManager = dataManager;
        this.cropDrops = cropDrops;
        this.messagesData = messagesData;
        this.messagesHandler = messagesHandler;
        this.multiCropHandler = multiCropHandler;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPistonExtend(BlockPistonRetractEvent event){
        List<Block> pushedBlocks = event.getBlocks();
        for(Block block : pushedBlocks) {
            if(isCrop(BlockUtil.getLocation(event.getDirection(), block))){
                if(!simpleCrops.getConfig().getBoolean("mehanics.piston-retract")){
                    event.setCancelled(true);
                }else{
                    handle(BlockUtil.getLocation(event.getDirection(), block));
                }
            }
            if(isCrop(BlockUtil.getLocation(BlockFace.UP, block))){
                if(!handle(BlockUtil.getLocation(BlockFace.UP, block))){
                    event.setCancelled(true);
                }
            }
            if(isCrop(block)){
                if(!simpleCrops.getConfig().getBoolean("mehanics.piston-retract")){
                    event.setCancelled(true);
                }else{
                    handle(block);
                }
            }
        }
    }

    public boolean isCrop(Block block){
        Block baseBlock = multiCropHandler.getBaseBlock(block);
        CropLocation cropLocation = new CropLocation(baseBlock.getWorld().toString(), baseBlock.getX(), baseBlock.getY(), baseBlock.getZ());
        Crop crop = dataManager.getCrop(cropLocation);
        if(crop == null){
            CropLocation nCropLocation = new CropLocation(block.getWorld().toString(), block.getX(), block.getY(), block.getZ());
            crop = dataManager.getCrop(nCropLocation);
        }
        return crop != null;
    }

    public boolean handle(Block block){
        if(multiCropHandler.isCrop(block)){
            Block baseBlock = multiCropHandler.getBaseBlock(block);
            if(multiCropHandler.isMultiCrop(baseBlock)){
                CropLocation cropLocation = new CropLocation(baseBlock.getWorld().toString(), baseBlock.getX(), baseBlock.getY(), baseBlock.getZ());
                Crop crop = dataManager.getCrop(cropLocation);
                if(crop != null){
                    if(!simpleCrops.getConfig().getBoolean("mehanics.piston-retract")) {
                        return false;
                    }
                    ArrayList<Block> cropBlocks = multiCropHandler.getCropBlocks(block);
                    Player player = Bukkit.getPlayer(crop.getPlacedBy());
                    String startMaterial = baseBlock.getType().toString();
                    for (Block cblock : cropBlocks) {
                        if(cblock.getY() != baseBlock.getY()) {
                            cropDrops.dropDrops(cblock, crop, player);
                            messagesHandler.debug(player, cblock, "cropdrops");
                        }
                        cblock.setType(Material.AIR);
                    }
                    if(!startMaterial.equalsIgnoreCase(baseBlock.getType().toString())) {
                        cropDrops.dropSeed(baseBlock, crop, player, false);
                        messagesHandler.debug(player, baseBlock, "seeddrop");
                        dataManager.removeCrop(cropLocation, crop);
                    }
                    return true;
                }
            }else{
                CropLocation cropLocation = new CropLocation(block.getWorld().toString(), block.getX(), block.getY(), block.getZ());
                Crop crop = dataManager.getCrop(cropLocation);
                if(crop != null){
                    if(!simpleCrops.getConfig().getBoolean("mehanics.piston-retract")) {
                        return false;
                    }
                    Player player = Bukkit.getPlayer(crop.getPlacedBy());
                    cropDrops.dropSeed(baseBlock, crop, player, false);
                    messagesHandler.debug(player, baseBlock, "seeddrop");
                    if(cropDrops.isFullyGrown(block)){
                        cropDrops.dropDrops(block, crop, player);
                        messagesHandler.debug(player, baseBlock, "cropdrops");
                    }
                    dataManager.removeCrop(cropLocation, crop);
                    block.setType(Material.AIR);
                    return true;
                }
            }
        }
        return false;
    }
}

