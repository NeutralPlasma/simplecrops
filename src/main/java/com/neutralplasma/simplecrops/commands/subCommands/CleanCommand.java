package com.neutralplasma.simplecrops.commands.subCommands;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.commands.CommandInterface;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CleanCommand implements CommandInterface {

    private SimpleCrops simpleCrops;
    private DataManager dataManager;
    private MessagesData messagesData;

    public CleanCommand(SimpleCrops simpleCrops, MessagesData messagesData, DataManager dataManager){
        this.simpleCrops = simpleCrops;
        this.messagesData = messagesData;
        this.dataManager = dataManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(sender.hasPermission("simplecrops.command.clean")){
            if(!(args.length > 1)){
                sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("noPlayer")));
                return true;
            }
            Player player = getPlayer(args[1]);
            if(player == null){
                sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("noPlayer")));
                return true;
            }
            if(dataManager.cleanPlayer(player.getUniqueId().toString())){
                String playername = player.getName();
                sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("cleanCommand.successfully").replace("{0}", playername)));
            }else{
                sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("cleanCommand.error").replace("{0}", args[1])));
            }


        }else{
            sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("noPermission").replace("{0}", "simplecrops.command.reload")));
            return true;
        }
        return false;
    }

    public Player getPlayer(String playername){
        return Bukkit.getPlayer(playername);
    }
}
