package com.neutralplasma.simplecrops.commands.subCommands;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.commands.CommandInterface;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.utils.CropDrops;
import com.neutralplasma.simplecrops.utils.ItemUtil;
import com.neutralplasma.simplecrops.utils.NBTDataUtil;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GiveCommand implements CommandInterface {

    private SimpleCrops simpleCrops;
    private CropDrops cropDrops;
    private NBTDataUtil nbtDataUtil;
    private MessagesData messagesData;
    private ItemUtil itemUtil;

    public GiveCommand(CropDrops cropDrops, NBTDataUtil nbtDataUtil, SimpleCrops simpleCrops, MessagesData messagesData, ItemUtil itemUtil){
        this.cropDrops = cropDrops;
        this.nbtDataUtil = nbtDataUtil;
        this.simpleCrops = simpleCrops;
        this.messagesData = messagesData;
        this.itemUtil = itemUtil;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(sender.hasPermission("simplecrops.command.give")){
            if(!(args.length > 1)){
                sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("noPlayer")));
                return true;
            }
            Player player = getPlayer(args[1]);

            if(player != null){
                if(args.length > 2){ // checks if user entered seedName
                    String seedname = args[2];
                    String gain = "1";
                    String strength = "1";
                    if(args.length > 3){ // checks if user entered gain.
                        gain = args[3];
                        int maxgain = simpleCrops.getFileManager().getConfiguration("configuration/crops").getInt("crops." + seedname + ".gain.max");
                        int mingain = simpleCrops.getFileManager().getConfiguration("configuration/crops").getInt("crops." + seedname + ".gain.min");
                        if(Integer.valueOf(gain) > maxgain){
                            gain = String.valueOf(maxgain);
                        }
                        if(Integer.valueOf(gain) < mingain){
                            gain = String.valueOf(mingain);
                        }
                    }else{
                        sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("missingArgs").replace("{0}", "Gain").replace("{1}", gain)));
                    }
                    if(args.length > 4){ // checks if user entered strength
                        strength = args[4];
                        int maxstrength = simpleCrops.getFileManager().getConfiguration("configuration/crops").getInt("crops." + seedname + ".strength.max");
                        int minstrength = simpleCrops.getFileManager().getConfiguration("configuration/crops").getInt("crops." + seedname + ".strength.min");
                        if(Integer.valueOf(strength) > maxstrength){
                            strength = String.valueOf(maxstrength);
                        }
                        if(Integer.valueOf(strength) < minstrength){
                            strength = String.valueOf(minstrength);
                        }
                    }else{
                        sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("missingArgs").replace("{0}", "Strength").replace("{1}", strength)));
                    }
                    String name;
                    name = simpleCrops.getFileManager().getConfiguration("configuration/crops").getString("crops." + seedname + ".name");
                    if(name == null){
                        sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("giveCommand.unknownCrop").replace("{0}", args[2])));
                        return true;
                    }
                    //String seedType = simpleCrops.getConfig().getString("seeds." + seedname + ".seed-type");
                    ItemStack item = cropDrops.getSeed(seedname); //itemUtil.decodeItem(simpleCrops.getConfig().getString("seeds." + seedname + ".seed-type"));

                    ItemStack crop = nbtDataUtil.createSeed(item, Integer.valueOf(gain), Integer.valueOf(strength), item.getItemMeta().getDisplayName(), seedname);

                    player.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("giveCommand.cropGiven").replace("{0}", name)));
                    player.getInventory().addItem(crop);
                }else{
                    sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("missingArg").replace("{0}", "seedName")));
                    return true;
                }
            }else{
                sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("unknownPlayer").replace("{0}", args[1])));
                return true;
            }

        }else{
            sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("noPermission").replace("{0}", "simplecrops.command.give")));
            return true;
        }
        return true;
    }

    public Player getPlayer(String playername){
        return Bukkit.getPlayer(playername);
    }


}
