package com.neutralplasma.simplecrops.commands.subCommands;

import com.neutralplasma.simplecrops.commands.CommandInterface;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.gui.Handler;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EditorCommand implements CommandInterface {
    private MessagesData messagesData;
    private Handler handler;

    public EditorCommand(Handler handler, MessagesData messagesData){
        this.handler = handler;
        this.messagesData = messagesData;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(sender instanceof Player) {
            Player player = (Player) sender;
            if(player.hasPermission("simplecrops.command.editor")) {
                handler.openCropsMenu(1, (Player) sender);
            }else {
                sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("noPermission").replace("{0}", "simplecrops.command.editor")));
                return true;
            }
        }else {
            sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("mustBePlayer")));
        }

        return true;
    }
}
