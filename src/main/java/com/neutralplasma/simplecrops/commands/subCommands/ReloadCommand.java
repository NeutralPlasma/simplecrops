package com.neutralplasma.simplecrops.commands.subCommands;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.commands.CommandInterface;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ReloadCommand implements CommandInterface {

    private SimpleCrops simpleCrops;
    private MessagesData messagesData;

    public ReloadCommand(SimpleCrops simpleCrops, MessagesData messagesData){
        this.simpleCrops = simpleCrops;
        this.messagesData = messagesData;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(sender.hasPermission("simplecrops.command.reload")){
            simpleCrops.reload();
            sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("reloadCommand.successfully")));
        }else{

            sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("noPermission").replace("{0}", "simplecrops.command.reload")));
            return true;
        }
        return false;
    }
}
