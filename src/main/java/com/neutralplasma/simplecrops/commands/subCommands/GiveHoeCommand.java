package com.neutralplasma.simplecrops.commands.subCommands;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.commands.CommandInterface;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.NBTDataUtil;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GiveHoeCommand implements CommandInterface {
    private SimpleCrops simpleCrops;
    private NBTDataUtil nbtDataUtil;
    private MessagesData messagesData;
    private MessagesHandler messagesHandler;

    public GiveHoeCommand(SimpleCrops simpleCrops, NBTDataUtil nbtDataUtil, MessagesData messagesData,
                          MessagesHandler messagesHandler){
        this.simpleCrops = simpleCrops;
        this.nbtDataUtil = nbtDataUtil;
        this.messagesData = messagesData;
        this.messagesHandler = messagesHandler;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(sender.hasPermission("simplecrops.command.givehoe")){
            if(!(args.length > 1)){
                sender.sendMessage(messagesHandler.getMessage("noPlayer"));
                return true;
            }
            Player player = getPlayer(args[1]);
            if(player != null){
                if(args.length > 2){
                    String hoeName = args[2];
                    String hoeMaterial = simpleCrops.getFileManager().getConfiguration("configuration/hoes").getString("hoes." + hoeName + ".item"); //simpleCrops.getConfig().getString("hoe.hoes." + hoeName + ".item");
                    if(hoeMaterial == null){
                        sender.sendMessage(messagesHandler.getMessage("hoe.unknownHoe").replace("{hoe}", hoeName));
                        return true;
                    }
                    int amount = 1;
                    if(args.length > 3){
                        amount = Integer.parseInt(args[3]);
                    }else{
                        sender.sendMessage(messagesHandler.getMessage("hoe.amountNotSpecified"));
                    }
                    String[] splittedMat = hoeMaterial.split(":");
                    if(splittedMat[0].equalsIgnoreCase("default")){
                        Material mat = Material.getMaterial(splittedMat[1]);
                        if(mat != null){
                            int size = simpleCrops.getFileManager().getConfiguration("configuration/hoes").getInt("hoes." + hoeName + ".size");
                            int uses = simpleCrops.getFileManager().getConfiguration("configuration/hoes").getInt("hoes." + hoeName + ".uses");
                            ItemStack item = new ItemStack(mat);
                            item = nbtDataUtil.createHoe(item, size, hoeName, uses);

                            item.setAmount(amount);
                            String hoeLabel = simpleCrops.getFileManager().getConfiguration("configuration/hoes").getString("hoes." + hoeName + ".name");
                            player.sendMessage(messagesHandler.getMessage("hoe.givenHoe")
                                    .replace("{hoe}", TextUtil.colorFormat(hoeLabel))
                                    .replace("{id}", hoeName)
                                    .replace("{giver}", sender.getName()));
                            sender.sendMessage(messagesHandler.getMessage("hoe.givenTo")
                                    .replace("{hoe}", TextUtil.colorFormat(hoeLabel))
                                    .replace("{id}", hoeName)
                                    .replace("{target}", player.getName()));
                            player.getInventory().addItem(item);
                        }else{
                            sender.sendMessage(messagesHandler.getMessage("hoe.wrongMaterial").replace("{material}", splittedMat[1]));
                        }

                    }else if(splittedMat[0].equalsIgnoreCase("custom")){

                    }
                }else{
                    sender.sendMessage(messagesHandler.getMessage("hoe.noHoe"));
                    return true;
                }
            }else{
                sender.sendMessage(messagesHandler.getMessage("unknownPlayer").replace("{0}", args[1]));
                return true;
            }
        }else{
            sender.sendMessage(messagesHandler.getMessage("noPermission").replace("{0}", "simplecrops.command.give"));
            return true;
        }
        return false;
    }

    public Player getPlayer(String playername){
        return Bukkit.getPlayer(playername);
    }
}
