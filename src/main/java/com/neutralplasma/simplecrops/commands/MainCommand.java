package com.neutralplasma.simplecrops.commands;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.gui.Handler;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class MainCommand implements CommandInterface{

    private SimpleCrops simpleCrops;
    private MessagesData messagesData;
    private Handler handler;

    public MainCommand(SimpleCrops simpleCrops, Handler handler, MessagesData messagesData){
        this.simpleCrops = simpleCrops;
        this.handler = handler;
        this.messagesData = messagesData;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        if (sender.hasPermission("")) {

        }else {
            sender.sendMessage(TextUtil.colorFormat(messagesData.getMessages().getString("noPermission").replace("{0}", "simplecrops.command.editor")));
            return true;
        }

        return true;
    }


}
