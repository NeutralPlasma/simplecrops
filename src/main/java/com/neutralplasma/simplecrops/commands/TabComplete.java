package com.neutralplasma.simplecrops.commands;

import com.neutralplasma.simplecrops.SimpleCrops;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class TabComplete implements TabCompleter {
    private SimpleCrops simpleCrops;

    public TabComplete(SimpleCrops simpleCrops){
        this.simpleCrops = simpleCrops;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
        if(command.getName().equalsIgnoreCase("simplecrops")){
            List<String> players = new ArrayList<>();
            //get online players
            for(Player player : Bukkit.getOnlinePlayers()){
                players.add(player.getName());
            }
            List<String> listedPlayers = new ArrayList<>();
            // player stuff.
            if(args.length > 1){
                if(args[0].equalsIgnoreCase("give") && commandSender.hasPermission("simplecrops.command.give")){
                    if(args.length == 2) {
                        for (String player : players) {
                            String lplayer;
                            lplayer = player.toLowerCase();
                            if (player.contains(args[1]) || lplayer.contains(args[1])) {
                                listedPlayers.add(player);
                            }
                        }
                        return listedPlayers;
                    }
                    if(args.length == 3){
                        List<String> seeds = new ArrayList<>();
                        ConfigurationSection configurationSection = simpleCrops.getFileManager().getConfiguration("configuration/crops").getConfigurationSection("crops");
                        for(String entered : configurationSection.getKeys(true)){
                            String lEntered;
                            lEntered = entered.toLowerCase();
                            if(entered.contains(args[2]) || lEntered.contains(args[2])){
                                if(!entered.contains(".")){
                                    seeds.add(entered);
                                }
                            }
                        }
                        return seeds;
                    }
                }

                if(args[0].equalsIgnoreCase("givehoe") && commandSender.hasPermission("simplecrops.command.givehoe")){
                    if(args.length == 2) {
                        for (String player : players) {
                            String lplayer;
                            lplayer = player.toLowerCase();
                            if (player.contains(args[1]) || lplayer.contains(args[1])) {
                                listedPlayers.add(player);
                            }
                        }
                        return listedPlayers;
                    }
                    if(args.length == 3){
                        List<String> seeds = new ArrayList<>();
                        ConfigurationSection configurationSection = simpleCrops.getFileManager().getConfiguration("configuration/hoes").getConfigurationSection("hoes");
                        for(String entered : configurationSection.getKeys(true)){
                            String lEntered;
                            lEntered = entered.toLowerCase();
                            if(entered.contains(args[2]) || lEntered.contains(args[2])){
                                if(!entered.contains(".")){
                                    seeds.add(entered);
                                }
                            }
                        }
                        return seeds;
                    }
                }


                if(args[0].equalsIgnoreCase("clean") && commandSender.hasPermission("simplecrops.command.clean")){
                    if(args.length > 2) {
                        for (String player : players) {
                            String lplayer;
                            lplayer = player.toLowerCase();
                            if (player.contains(args[1]) || lplayer.contains(args[1])) {
                                listedPlayers.add(player);
                            }
                        }
                    }
                    return listedPlayers;
                }
            }



            else{ // subcommand stuff.
                List<String> possible = new ArrayList<>();
                List<String> has = new ArrayList<>();
                if(commandSender.hasPermission("simplecrops.command.reload")){
                    possible.add("reload");
                }
                if(commandSender.hasPermission("simplecrops.command.give")){
                    possible.add("give");
                }
                if(commandSender.hasPermission("simplecrops.command.givehoe")){
                    possible.add("givehoe");
                }
                if(commandSender.hasPermission("simplecrops.command.clean")){
                    possible.add("clean");
                }
                if(commandSender.hasPermission("simplecrops.command.editor")){
                    possible.add("editor");
                }
                for(String entered : possible){
                    String lentered = entered.toLowerCase();
                    if(entered.contains(args[0]) || lentered.contains(args[0])){
                        has.add(entered);
                    }
                }
                return has;
            }
        }
        return null;
    }
}
