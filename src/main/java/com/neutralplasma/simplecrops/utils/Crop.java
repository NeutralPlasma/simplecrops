package com.neutralplasma.simplecrops.utils;

import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;

public class Crop {

    private String name;
    private int gain;
    private int strength;
    private int bonemeal = 0;
    private UUID placedBy;
    private String id;

    public Crop(String name, int gain, int strength, UUID placedBy, String id) {
        this.name = name;
        this.gain = gain;
        this.strength = strength;
        this.placedBy = placedBy;
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public int getGain(){
        return gain;
    }
    public int getStrength(){
        return strength;
    }
    public UUID getPlacedBy(){
        return placedBy;
    }
    public String getid(){
        return id;
    }
    public int getBonemeal(){
        return this.bonemeal;
    }


    public void setBonemeal(int bonemeal){
        this.bonemeal = bonemeal;
    }
    public void setGain(int gain){
        this.gain = gain;
    }
    public void setStrength(int strength){
        this.strength = strength;
    }

    public void addBoneMeal(int toadd){
        this.bonemeal = this.bonemeal + toadd;
    }
}
