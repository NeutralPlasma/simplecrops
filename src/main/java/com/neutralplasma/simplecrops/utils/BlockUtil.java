package com.neutralplasma.simplecrops.utils;

import com.google.common.collect.Lists;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Directional;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class BlockUtil {
    public static List<Block> getSquare(Block start, int radius){
        if (radius < 0) {
            return new ArrayList<>(0);
        }
        int iterations = (radius * 2) + 1;
        List<Block> blocks = new ArrayList<>(iterations * iterations * iterations);
        for (int x = -radius; x <= radius; x++) {
            for (int y = -radius; y <= radius; y++) {
                for (int z = -radius; z <= radius; z++) {
                    blocks.add(start.getRelative(x, y, z));
                }
            }
        }
        return blocks;
    }

    public static List<Block> getFlatSquare(Block start, int radius){
        if (radius < 0) {
            return new ArrayList<>(0);
        }
        int iterations = (radius * 2) + 1;
        List<Block> blocks = new ArrayList<>(iterations * iterations);
        int y = start.getY();
        for (int x = -radius; x <= radius; x++) {
            for (int z = -radius; z <= radius; z++) {
                blocks.add(start.getRelative(x, y, z));
            }

        }
        return blocks;
    }

    public static Block getStemBlock(Block base, Material type){
        Block nearby = base.getRelative(BlockFace.EAST);
        if(nearby.getType() == type){
            Directional d = (Directional) nearby.getBlockData();
            if(d.getFacing() == BlockFace.WEST)
            return base.getRelative(BlockFace.EAST);
        }
        nearby = base.getRelative(BlockFace.WEST);
        if(nearby.getType() == type){
            Directional d = (Directional) nearby.getBlockData();
            if(d.getFacing() == BlockFace.EAST)
                return base.getRelative(BlockFace.WEST);
        }
        nearby = base.getRelative(BlockFace.NORTH);
        if(nearby.getType() == type){
            Directional d = (Directional) nearby.getBlockData();
            if(d.getFacing() == BlockFace.SOUTH)
                return base.getRelative(BlockFace.NORTH);
        }
        nearby = base.getRelative(BlockFace.SOUTH);
        if(nearby.getType() == type){
            Directional d = (Directional) nearby.getBlockData();
            if(d.getFacing() == BlockFace.NORTH)
                return base.getRelative(BlockFace.SOUTH);
        }
        return null;
    }

    public static Block getLocation(BlockFace face, Block block){

        Location nextLocation = block.getLocation();

        switch (face) {
            case SOUTH :
                nextLocation.setZ(nextLocation.getZ() + 1);
                break;
            case UP:
                nextLocation.setY(nextLocation.getY() + 1);
                break;
            case DOWN:
                nextLocation.setY(nextLocation.getY() - 1);
                break;
            case EAST:
                nextLocation.setX(nextLocation.getX() + 1);
                break;
            case WEST:
                nextLocation.setX(nextLocation.getX() - 1);
                break;
            case NORTH:
                nextLocation.setZ(nextLocation.getZ() - 1);
                break;
        }

        return block.getWorld().getBlockAt(nextLocation);
    }

}
