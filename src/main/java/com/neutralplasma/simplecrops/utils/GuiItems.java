package com.neutralplasma.simplecrops.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GuiItems {


    public static ItemStack getGlass(int type) {
        return GuiItems.getGlass(type, Bukkit.getVersion());
    }

    public static ItemStack getWool(int type){
        return GuiItems.getWool(type, Bukkit.getVersion());
    }


    private static ItemStack getGlass(int type, String version) {
        ItemStack glass;
        glass = new ItemStack(notLegacy(version) ?
                    Material.LEGACY_STAINED_GLASS_PANE : Material.valueOf("STAINED_GLASS_PANE"), 1, (short) type);

        ItemMeta glassmeta = glass.getItemMeta();
        glassmeta.setDisplayName("§l");
        glass.setItemMeta(glassmeta);
        return glass;
    }
    public static boolean notLegacy(String version){
        if(version.contains("1.8") || version.contains("1.9") || version.contains("1.10") || version.contains("1.11")){
            return false;
        }else{
            return true;
        }
    }

    private static ItemStack getWool(int type, String version) {
        ItemStack wool;
        wool = new ItemStack(notLegacy(version) ?
                Material.LEGACY_WOOL : Material.valueOf("WOOL"), 1, (short) type);

        ItemMeta woolmeta = wool.getItemMeta();
        woolmeta.setDisplayName("§l");
        wool.setItemMeta(woolmeta);
        return wool;
    }

}
