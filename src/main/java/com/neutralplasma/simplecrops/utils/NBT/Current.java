package com.neutralplasma.simplecrops.utils.NBT;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;

public class Current {
    private SimpleCrops simpleCrops;
    private MessagesHandler messagesHandler;

    public Current(SimpleCrops simpleCrops, MessagesHandler messagesHandler){
        this.simpleCrops = simpleCrops;
        this.messagesHandler = messagesHandler;
    }

    public Integer getInt(ItemStack item, String dataContainer){
        NamespacedKey key = new NamespacedKey(simpleCrops, dataContainer);
        ItemMeta itemMeta = item.getItemMeta();
        PersistentDataContainer tagContainer = itemMeta.getPersistentDataContainer();
        if(tagContainer.has(key, PersistentDataType.INTEGER)) {
            int foundValue = tagContainer.get(key, PersistentDataType.INTEGER);
            return foundValue;
        }
        return 0;
    }

    public String getString(ItemStack item, String dataContainer){
        NamespacedKey key = new NamespacedKey(simpleCrops, dataContainer);
        ItemMeta itemMeta = item.getItemMeta();
        PersistentDataContainer tagContainer = itemMeta.getPersistentDataContainer();
        if(tagContainer.has(key, PersistentDataType.STRING)) {
            String foundValue = tagContainer.get(key, PersistentDataType.STRING);
            return foundValue;
        }
        return "none";
    }

    public ItemMeta setInt(ItemMeta meta, int number, String dataContainer){
        NamespacedKey key = new NamespacedKey(simpleCrops, dataContainer);
        try {
            PersistentDataContainer tagContainer = meta.getPersistentDataContainer();
            tagContainer.set(key, PersistentDataType.INTEGER, number);
        }catch (NullPointerException error){
            error.getCause();
            error.fillInStackTrace();
        }
        return meta;
    }

    public ItemMeta setString(ItemMeta meta, String string, String dataContainer){
        NamespacedKey key = new NamespacedKey(simpleCrops, dataContainer);
        try {
            PersistentDataContainer tagContainer = meta.getPersistentDataContainer();
            tagContainer.set(key, PersistentDataType.STRING, string);
        }catch (NullPointerException error){
            error.getCause();
            error.fillInStackTrace();
        }
        return meta;
    }

    public ItemStack createSeed(ItemStack item, int gain, int strength, String name, String id){
        try {
            List<String> list;
            try {
                list = item.getItemMeta().getLore();
            }catch (NullPointerException error){
                list = null;
            }
            List<String> lore = new ArrayList<>();
            ItemMeta meta = item.getItemMeta();
            meta = setInt(meta, gain, "gain");
            meta = setInt(meta, strength, "strength");
            meta = setString(meta, id, "id");
            meta.setDisplayName(TextUtil.colorFormat(simpleCrops.getConfig().getString("seed-format.name").replace("{0}", name)));
            if(list != null) {
                for (Object object : list.toArray()) {
                    String loreline = object.toString();
                    if (loreline.contains("{strength}") || loreline.contains("{gain}")) {
                        loreline = loreline.replace("{strength}", String.valueOf(strength));
                        loreline = loreline.replace("{gain}", String.valueOf(gain));
                    }
                    lore.add(TextUtil.colorFormat(loreline));
                }
                meta.setLore(lore);
            }
            item.setItemMeta(meta);
            return item;
        }catch (NullPointerException error){
            error.getCause();
            return null;
        }
    }

    public ItemStack createHoe(ItemStack item, int size, String id, int uses){
        try {

            ItemMeta meta = item.getItemMeta();
            meta = setInt(meta, size, "HarvestSize");
            meta = setString(meta, id, "id");
            meta = setInt(meta, uses, "Uses");
            meta.setDisplayName(TextUtil.colorFormat(simpleCrops.getFileManager().getConfiguration("configuration/hoes").getString("hoes." + id + ".name")));

            meta.setLore(messagesHandler.formatList(
                    simpleCrops.getFileManager().getConfiguration("configuration/hoes").getStringList("hoes." + id + ".lore"),
                    new String[]{"{uses}", "{size}"}, new String[]{String.valueOf(uses), String.valueOf(size)}
            ));

            item.setItemMeta(meta);
            return item;
        }catch (NullPointerException error){
            error.getCause();
            return null;
        }
    }
}
