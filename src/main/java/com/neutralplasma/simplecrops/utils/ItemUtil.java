package com.neutralplasma.simplecrops.utils;

import com.neutralplasma.simplecrops.SimpleCrops;
import eu.virtusdevelops.virtuscore.managers.FileManager;
import eu.virtusdevelops.virtuscore.utils.BlockUtil;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Base64;
import java.util.List;

public class ItemUtil {

    public String encodeItem(ItemStack itemStack) {
        YamlConfiguration yamlConfiguration = new YamlConfiguration();
        yamlConfiguration.set("item", itemStack);
        return Base64.getEncoder().encodeToString(yamlConfiguration.saveToString().getBytes());
    }

    public ItemStack decodeItem(String encodedItem) {
        YamlConfiguration yamlConfiguration = new YamlConfiguration();

        try {
            yamlConfiguration.loadFromString(new String(Base64.getDecoder().decode(encodedItem)));
        } catch (InvalidConfigurationException e) {
            Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&cError: " + e));
            return null;
        }

        return yamlConfiguration.getItemStack("item");
    }

    public ItemStack getItem(String path, FileManager fileManager){
        String name = fileManager.getConfiguration("configuration/items").getString("items." + path + ".name");
        List<String> lore = fileManager.getConfiguration("configuration/items").getStringList("items." + path + ".lore");
        Material mat = Material.getMaterial(fileManager.getConfiguration("configuration/items").getString("items." + path + ".material"));
        ItemStack item = new ItemStack(mat);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(TextUtil.colorFormat(name));
        meta.setLore(TextUtil.colorFormatList(lore));
        item.setItemMeta(meta);
        return item;

    }
}
