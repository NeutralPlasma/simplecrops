package com.neutralplasma.simplecrops.utils;

import org.bukkit.World;

public class CropLocation {
    private String world;
    private long x;
    private long y;
    private long z;

    public CropLocation(String world, long x, long y, long z){
        this.world = world;
        this.x = x;
        this.z = z;
        this.y = y;
    }
    public CropLocation(World world, long x, long y, long z){
        this.world = world.getName();
        this.x = x;
        this.z = z;
        this.y = y;
    }

    public String getWorld(){
        return world;
    }
    public long getX(){
        return x;
    }
    public long getY(){
        return y;
    }
    public long getZ(){
        return z;
    }

}
