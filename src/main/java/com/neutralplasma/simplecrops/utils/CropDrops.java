package com.neutralplasma.simplecrops.utils;

import com.google.gson.Gson;
import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.events.crop.LevelUpData;
import com.neutralplasma.simplecrops.handlers.CropLeveling;
import eu.virtusdevelops.virtuscore.VirtusCore;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class CropDrops {

    private SimpleCrops simpleCrops;
    private NBTDataUtil dataUtil;
    private CropLeveling cropLeveling;
    private ItemUtil itemUtil;

    public CropDrops(SimpleCrops simpleCrops, NBTDataUtil dataUtil, CropLeveling cropLeveling,
                     ItemUtil itemUtil) {
        this.cropLeveling = cropLeveling;
        this.simpleCrops = simpleCrops;
        this.dataUtil = dataUtil;
        this.itemUtil = itemUtil;
    }

    private List<String> getCommandDrops(Crop crop, Player player){
        List<String> commands = new ArrayList<>();
        if(player == null){
            return commands;
        }
        if (crop == null) {
            return null;
        } else {
            String id = crop.getid();
            List<String> rawcommands = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + id + ".harvest.commands");
            if(rawcommands == null){
                return commands;
            }
            for(String command : rawcommands){
                command = command.replace("%p", player.getName());
                commands.add(command);
            }
            return commands;

        }
    }

    private void sendMessageDrops(Crop crop, Player player){
        if (crop != null) {
            String id = crop.getid();
            List<String> messages = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + id + ".harvest.messages");
            for(String message : messages){
                message = TextUtil.formatString(message, "%cropname:" + crop.getName(), "%cropid:" + crop.getid(), "%p:" + player.getName());

                if(message.startsWith("[player]")) {
                    message = message.replace("[player]", "");

                    player.sendMessage(TextUtil.colorFormat(message));

                }
                if(message.startsWith("[console]")){
                    message = message.replace("[console]", "");
                    Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat(message));
                }
                if(message.startsWith("[broadcast]")){
                    message = message.replace("[broadcast]", "");
                    Bukkit.broadcast(TextUtil.colorFormat(message),  "simplecrops.drops.broadcast");
                }
            }
        }
    }


    public List<ItemStack> getDrops(Crop crop) {
        Random random = new Random();
        if (crop == null) {
            return null;
        } else {
            ArrayList<ItemStack> itemDrops = new ArrayList<>();
            String id = crop.getid();
            List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + id + ".harvest.items");
            for (String material : drops) {
                String[] results = material.split(":");
                if(results[0].equalsIgnoreCase("default")){
                    if (results[1] != null) {
                        ItemStack item = new ItemStack(Material.getMaterial(results[1]));
                        int number = random.nextInt(Integer.parseInt(results[3]) - Integer.parseInt(results[2]) + 2) + Integer.parseInt(results[2]);
                        number += crop.getGain();
                        item.setAmount(number);
                        itemDrops.add(item);
                    }
                }else{
                    ItemStack item = itemUtil.decodeItem(results[1]);
                    int number = random.nextInt(Integer.parseInt(results[3]) - Integer.parseInt(results[2]) + 2) + Integer.parseInt(results[2]);
                    number += crop.getGain();
                    item.setAmount(number);
                    itemDrops.add(item);
                }
            }
            return itemDrops;
        }
    }

    public boolean dropSeed(Block block, Crop crop, Player player, boolean fullygrown){
        boolean shouldLevelUp = simpleCrops.getConfig().getBoolean("mehanics.cropleveling");
        if(!shouldLevelUp){
            fullygrown = false;
        }
        if(player != null) {
            if (player.hasPermission("simplecrops.drops.seed")) {
                block.getWorld().dropItemNaturally(block.getLocation(), getSeed(crop, fullygrown, player));
                return true;
            }
        }else {
            block.getWorld().dropItemNaturally(block.getLocation(), getSeed(crop, fullygrown));
            return true;
        }
        return false;
    }

    public boolean isFullyGrown(Block block){
        if(block.getType().toString().equalsIgnoreCase("SUGAR_CANE")
                || block.getType().toString().equalsIgnoreCase("CACTUS")
                || block.getType().toString().equalsIgnoreCase("BAMBOO")
                || block.getType().toString().equalsIgnoreCase("BAMBOO_SAPLING")
                || block.getType().toString().equalsIgnoreCase("KELP")
                || block.getType().toString().equalsIgnoreCase("KELP_PLANT")
                || block.getType().toString().equalsIgnoreCase("AIR")){
            return true;
        }
        BlockData bdata = block.getBlockData();
        if(bdata instanceof Ageable){
            Ageable age = (Ageable) bdata;
            if(age.getAge() == age.getMaximumAge()){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }



    public void setGrowth(Block block, String age){
        BlockData bdata = block.getBlockData();
        if(bdata instanceof Ageable){
            Ageable bage = (Ageable) bdata;
            int max = bage.getMaximumAge();
            if(age.equalsIgnoreCase("first")){
                bage.setAge(0);
            }else if(age.equalsIgnoreCase("second")){
                bage.setAge(max/2);
            }else if(age.equalsIgnoreCase("third")){
                bage.setAge(max);
            }
            block.setBlockData(bdata);
        }
    }

    public boolean dropDrops(Block block, Crop crop, Player player){
        try{
            if(isFullyGrown(block)){
                List<ItemStack> drops = getDrops(crop);
                List<String> commands = getCommandDrops(crop, player);
                // Item Drops
                if(player !=null){
                    if(player.hasPermission("simplecrops.drops.items")){
                        for(ItemStack item:drops){
                            block.getWorld().dropItemNaturally(block.getLocation(), item);
                        }
                    }
                }else{
                    for(ItemStack item:drops){
                        block.getWorld().dropItemNaturally(block.getLocation(), item);
                    }
                }
                // Command drops
                if(player !=null) {
                    if (player.hasPermission("simplecrops.drops.commands")) {
                        for (String command : commands) {
                            VirtusCore.executeCommand(command);
                        }
                    }
                }else{
                    for (String command : commands) {
                        VirtusCore.executeCommand(command);
                    }
                }
                // Message drops:
                sendMessageDrops(crop, player);
                return true;

            }
        }catch (Exception error){
            error.printStackTrace();
            return false;
        }
        return false;
    }



    public ItemStack getSeed(Crop crop, boolean fullygrown){
        ItemStack seedType = null;
        String seedString = simpleCrops.getFileManager().getConfiguration("configuration/crops").getString("crops." + crop.getid() + ".seed-type");
        String[] spliced = seedString.split(":");
        if(spliced[0].equalsIgnoreCase("custom")){
            seedType = itemUtil.decodeItem(spliced[1]);
        }else if(spliced[0].equalsIgnoreCase("default")){
            seedType = itemUtil.getItem(spliced[1], simpleCrops.getFileManager());
        }

        if(seedType == null){
            seedType = new ItemStack(XMaterial.WHEAT_SEEDS.parseMaterial(), 1);
        }

        if(fullygrown){
            crop = cropLeveling.levelUpCrop(crop);
        }

        return dataUtil.createSeed(seedType, crop.getGain(), crop.getStrength(), seedType.getItemMeta().getDisplayName(), crop.getid());
    }

    public ItemStack getSeed(String id){
        ItemStack seedType = null;
        String seedString = simpleCrops.getFileManager().getConfiguration("configuration/crops").getString("crops." + id + ".seed-type");
        String[] spliced = seedString.split(":");
        if(spliced[0].equalsIgnoreCase("custom")){
            seedType = itemUtil.decodeItem(spliced[1]);
        }else if(spliced[0].equalsIgnoreCase("default")){
            seedType = itemUtil.getItem(spliced[1], simpleCrops.getFileManager());
        }

        if(seedType == null){
            seedType = new ItemStack(XMaterial.WHEAT_SEEDS.parseMaterial(), 1);
        }


        return seedType;
    }

    public ItemStack getSeed(Crop crop, boolean fullygrown, Player player){
        ItemStack seedType = null;
        String seedString = simpleCrops.getFileManager().getConfiguration("configuration/crops").getString("crops." + crop.getid() + ".seed-type");
        String[] spliced = seedString.split(":");
        if(spliced[0].equalsIgnoreCase("custom")){
            seedType = itemUtil.decodeItem(spliced[1]);
        }else if(spliced[0].equalsIgnoreCase("default")){
            seedType = itemUtil.getItem(spliced[1], simpleCrops.getFileManager());
        }


        if(fullygrown){
            LevelUpData data = cropLeveling.levelUpCrop(crop, "DD");
            crop = data.getCrop();
            if(player != null && data.didLevelUP()) {
                player.getWorld().playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 15);
            }
        }

        return dataUtil.createSeed(seedType, crop.getGain(), crop.getStrength(), seedType.getItemMeta().getDisplayName(), crop.getid());
    }

    public boolean growBlocks(Crop crop, Block block, Player player){
        if(player != null) {
            if(player.hasPermission("simplecrops.drops.blocks")) {
                block.setType(randomChance(getChances(crop)));
                return true;
            }
        }
        return false;
    }

    private Map<Material, Double> getChances(Crop crop){
        HashMap<Material, Double> map = new HashMap<>();
        List<String> ores = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop.getid() + ".harvest.blocks");
        if(ores.size() > 0){
            for(String ore : ores){
                String[] splited = ore.split(":");
                if(splited.length > 1){
                    double chance = Double.parseDouble(splited[1]);
                    Material mat = Material.getMaterial(splited[0]);
                    if(mat != null){
                        map.put(mat, chance);
                    }
                }
            }
        }
        return map;
    }


    private Material randomChance(Map<Material, Double> chances) {
        final Random r = new Random();
        double chance = 100.0 * r.nextDouble();

        for (final Material material : chances.keySet()) {
            chance -= chances.get(material);
            if (chance <= 0.0) {
                return material;
            }
        }

        return Material.PUMPKIN;
    }

}
