package com.neutralplasma.simplecrops.utils;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.utils.NBT.Current;
import com.neutralplasma.simplecrops.utils.NBT.Legacy;
import eu.virtusdevelops.virtuscore.compatibility.ServerVersion;
import net.minecraft.server.v1_14_R1.DataWatcher;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class NBTDataUtil {

    private SimpleCrops simpleCrops;
    private Legacy legacy;
    private Current current;
    private boolean useLegacy = false;


    public NBTDataUtil(SimpleCrops simpleCrops, Legacy legacy, Current current) {
        this.simpleCrops = simpleCrops;
        this.legacy = legacy;
        this.current = current;

        if (ServerVersion.isServerVersionBelow(ServerVersion.V1_14)) {
            this.useLegacy = true;
        }

    }

    public Integer getInt(ItemStack item, String dataContainer){
        if(useLegacy){
            return legacy.getInt(item, dataContainer);
        }else{
            return current.getInt(item, dataContainer);
        }
    }

    public String getString(ItemStack item, String dataContainer){
        if(useLegacy){
            return legacy.getString(item, dataContainer);
        }else{
            return current.getString(item, dataContainer);
        }
    }

    public ItemMeta setInt(ItemStack item, String dataContainer, int value){
        if(useLegacy){
            return legacy.setInt(item.getItemMeta(),value, dataContainer);
        }else{
            return current.setInt(item.getItemMeta(),value, dataContainer);
        }
    }


    public ItemStack createSeed(ItemStack item, int gain, int strength, String name, String id){
        if (useLegacy){
            return legacy.createSeed(item, gain, strength, name, id);
        }else{
            return current.createSeed(item, gain, strength, name, id);
        }
    }

    public ItemStack createHoe(ItemStack item, int size, String id, int uses){
        if (useLegacy){
            return legacy.createHoe(item, size, id, uses);
        }else{
            return current.createHoe(item, size, id, uses);
        }
    }


}
