package com.neutralplasma.simplecrops.gui.subGuis;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.gui.Handler;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import eu.virtusdevelops.virtuscore.gui.Icon;
import eu.virtusdevelops.virtuscore.gui.InventoryCreator;
import eu.virtusdevelops.virtuscore.gui.actions.*;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import com.neutralplasma.simplecrops.utils.*;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomDropsGUI {
    private SimpleCrops simpleCrops;
    private Handler handler;
    private ItemUtil itemUtil;
    private MessagesHandler message;

    private List<String> itemsList = new ArrayList<>();
    private List<Integer> items = new ArrayList<>();
    private HashMap<Integer,Integer> itemsMap = new HashMap<>();
    private InventoryCreator inventoryCreator;
    private int currentpage = 1;
    private String crop= "";

    public CustomDropsGUI(SimpleCrops simpleCrops, Handler handler,
                          ItemUtil itemUtil, MessagesHandler messagesHandler){
        this.handler = handler;
        this.simpleCrops = simpleCrops;
        this.itemUtil = itemUtil;
        this.message = messagesHandler;
        this.inventoryCreator = new InventoryCreator(45, TextUtil.colorFormat("&bEdit item drops."));
    }


    public void openGUI(Player player, String crop, int page){
        inventoryCreator.clean();
        items.clear();
        currentpage = page;
        this.crop = crop;
        setupDrops();
        prevPage();
        nextPage();
        createNew();
        createItemIcons();
        setupCloseAction();
        //setup background
        setupBackground();

        player.openInventory(inventoryCreator.getInventory());
    }

    public void setupCloseAction(){
        inventoryCreator.addCloseActions(new InventoryCloseAction() {
            @Override
            public void execute(Player player, Inventory inventory) {
                handler.openCropEditMenu(player, crop,true);
            }
        });
    }

    public void setupDrops(){

        itemsList.clear();
        itemsMap.clear();
        List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop + ".harvest.items");
        //gets all item drops from config
        itemsList.addAll(drops);

        // Intializes pages for drops
        int page = 1;

        for(int x = 0; x < itemsList.size(); x++){
            if(x >= page*21){
                page++;
            }
            itemsMap.put(x, page);
        }
    }

    public void createItemIcons(){
        int itempos = 10;
        for(Integer x : itemsMap.keySet()){
            if(itemsMap.get(x) == currentpage){


                String[] results = itemsList.get(x).split(":");
                ItemStack citem = new ItemStack(Material.BARRIER);
                /*if(citem == null){
                    citem = new ItemStack(XMaterial.BARRIER.parseMaterial());
                }
                citem.setAmount(Integer.parseInt(item[2]));*/


                if(results[0].equalsIgnoreCase("default")){
                    if (results[1] != null) {
                        citem = new ItemStack(Material.getMaterial(results[1]));
                        citem.setAmount(Integer.parseInt(results[3]));
                    }
                }else{
                    citem = itemUtil.decodeItem(results[1]);
                    citem.setAmount(Integer.parseInt(results[3]));
                }


                Icon itemic = new Icon(citem);
                itemic.addRightClickAction(new RightClickAction() {
                    @Override
                    public void execute(Player player) {
                        List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop + ".harvest.items");
                        drops.remove(itemsList.get(x));
                        simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + crop + ".harvest.items", drops);
                        simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                        //simpleCrops.saveConfig();
                        openGUI(player, crop, currentpage);
                    }
                });
                itemic.addLeftClickAction(new LeftClickAction() {
                    @Override
                    public void execute(Player player) {
                        AbstractAnvilGUI anvilGUI = new AbstractAnvilGUI(player, event ->{
                            String entered = event.getName();
                            String[] stripped = entered.split(":");
                            if(stripped.length > 1){
                                int min = Integer.parseInt(stripped[0]);
                                int max = Integer.parseInt(stripped[1]);
                                List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop + ".harvest.items");
                                drops.remove(itemsList.get(x));
                                drops.add(results[0] + ":" + results[1] + ":" + min + ":" + max);
                                simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + crop + ".harvest.items", drops);
                                simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                                //simpleCrops.saveConfig();
                                //simpleCrops.reloadConfig();
                            }else{
                                player.sendMessage("Error! Please enter the following format: <min>:<max>!");
                            }
                        }, simpleCrops);
                        ItemStack itemp = new ItemStack(XMaterial.PAPER.parseMaterial());
                        ItemMeta meta = itemp.getItemMeta();
                        meta.setDisplayName("<MIN>:<MAX>");
                        itemp.setItemMeta(meta);
                        anvilGUI.setSlot(AbstractAnvilGUI.AnvilSlot.INPUT_LEFT, itemp);
                        anvilGUI.setOnClose((player1, inventory1) -> {
                            handler.openCustomDropsMenu(player1, crop,  currentpage);
                        });
                        anvilGUI.open();
                    }
                });
                items.add(itempos);
                inventoryCreator.setIcon(itempos, itemic);

                if(itempos == 16){
                    itempos = 19;
                }else if(itempos == 25){
                    itempos = 28;
                }else {
                    itempos++;
                }
            }
        }

    }

    public void nextPage(){
        ItemStack nextPageItem = new ItemStack(XMaterial.PAPER.parseMaterial());
        ItemMeta itemMeta = nextPageItem.getItemMeta();
        itemMeta.setDisplayName(TextUtil.colorFormat("&bNext Page"));
        nextPageItem.setItemMeta(itemMeta);
        Icon nextpage = new Icon(nextPageItem);
        nextpage.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                currentpage++;
                openGUI(player, crop, currentpage);
            }
        });
        items.add(43);
        inventoryCreator.setIcon(43, nextpage);
    }

    public void prevPage() {
        if (currentpage > 1 ) {
            ItemStack prevPage = new ItemStack(XMaterial.PAPER.parseMaterial());
            ItemMeta itemMeta = prevPage.getItemMeta();
            itemMeta.setDisplayName(TextUtil.colorFormat("&bPrevious Page"));
            prevPage.setItemMeta(itemMeta);
            Icon nextpage = new Icon(prevPage);
            nextpage.addClickAction(new ClickAction() {
                @Override
                public void execute(Player player) {
                    currentpage--;
                    openGUI(player, crop, currentpage);
                }
            });
            items.add(37);
            inventoryCreator.setIcon(37, nextpage);
        }
    }

    public void createNew(){
        ItemStack book = new ItemStack(XMaterial.BOOK.parseMaterial());
        ItemMeta itemMeta = book.getItemMeta();
        itemMeta.setDisplayName(TextUtil.colorFormat("&bAdd new"));
        List<String> lore = new ArrayList<>();
        lore.add("&7Put item in here to add it.");
        itemMeta.setLore(TextUtil.colorFormatList(lore));
        book.setItemMeta(itemMeta);
        Icon addItem = new Icon(book);
        addItem.addDragItemIntoAction(new DragItemIntoAction() {
            @Override
            public void execute(Player player, ItemStack item) {
                player.sendMessage(item.getType() + ":" + item.getAmount());
                if(item.getType() != Material.AIR){
                    ItemStack newItem = item.clone();
                    item.setAmount(0);
                    AbstractAnvilGUI anvilGUI = new AbstractAnvilGUI(player, event ->{
                        String entered = event.getName();
                        String[] stripped = entered.split(":");
                        if(stripped.length > 1){
                            int min = Integer.parseInt(stripped[0]);
                            int max = Integer.parseInt(stripped[1]);
                            List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop + ".harvest.items");
                            drops.add("custom:" + itemUtil.encodeItem(newItem) + ":" + min + ":" + max);
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + crop + ".harvest.items", drops);
                            simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                            //simpleCrops.saveConfig();
                            //simpleCrops.reloadConfig();
                        }else{
                            player.sendMessage("Error! Please enter the following format: <min>:<max>!");
                        }
                    }, simpleCrops);
                    ItemStack itemp = new ItemStack(XMaterial.PAPER.parseMaterial());
                    ItemMeta meta = itemp.getItemMeta();
                    meta.setDisplayName("<MIN>:<MAX>");
                    itemp.setItemMeta(meta);
                    anvilGUI.setSlot(AbstractAnvilGUI.AnvilSlot.INPUT_LEFT, itemp);
                    anvilGUI.setOnClose((player1, inventory1) -> {
                        openGUI(player1, crop,  currentpage);
                    });
                    anvilGUI.open();
                }else{
                    AbstractAnvilGUI gui = new AbstractAnvilGUI(player, event -> {
                        try {
                            List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop + ".harvest.items");
                            String[] splited = event.getName().toUpperCase().split(":");
                            if(splited.length > 2){
                                if(Material.getMaterial(splited[0]) != null) {
                                    drops.add("default:" + event.getName().toUpperCase());
                                    simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + crop + ".harvest.items", drops);
                                    simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                                    //simpleCrops.saveConfig();
                                }else{
                                    player.sendMessage(message.getMessage("gui.itemDropsGui.invalidMaterial".replace("{0}", splited[0])));
                                }
                            }else{
                                player.sendMessage(message.getMessage("gui.itemDropsGui.enterValidFormat".replace("{0}", event.getName())));
                            }
                        } catch (Exception error){
                            player.sendMessage(TextUtil.colorFormat("&cError! Check console."));
                            error.printStackTrace();
                        }
                    }, simpleCrops);
                    ItemStack itemp = new ItemStack(XMaterial.PAPER.parseMaterial());
                    ItemMeta meta = itemp.getItemMeta();
                    meta.setDisplayName("<MATERIAL>:<MIN>:<MAX>");
                    itemp.setItemMeta(meta);
                    gui.setSlot(AbstractAnvilGUI.AnvilSlot.INPUT_LEFT, itemp);
                    gui.setOnClose((player1, inventory1) -> {
                        openGUI(player1, crop,  currentpage);
                    });
                    gui.open();
                }
            }
        });
        items.add(41);
        inventoryCreator.setIcon(41, addItem);
    }

    public Icon getBackGround(int glassid){
        ItemStack glass;
        glass = new ItemStack(GuiItems.getGlass(glassid));
        ItemMeta meta = glass.getItemMeta();
        meta.setDisplayName("");
        glass.setItemMeta(meta);
        Icon background = new Icon(glass);
        background.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
            }
        });
        return background;
    }

    public void setupBackground(){
        inventoryCreator.setIcon(0, getBackGround(2));
        inventoryCreator.setIcon(1, getBackGround(2));
        inventoryCreator.setIcon(7, getBackGround(2));
        inventoryCreator.setIcon(8, getBackGround(2));
        inventoryCreator.setIcon(9, getBackGround(2));
        inventoryCreator.setIcon(17, getBackGround(2));
        inventoryCreator.setIcon(27, getBackGround(2));
        inventoryCreator.setIcon(35, getBackGround(2));
        inventoryCreator.setIcon(36, getBackGround(2));
        inventoryCreator.setIcon(44, getBackGround(2));
        items.add(0);
        items.add(1);
        items.add(7);
        items.add(8);
        items.add(9);
        items.add(17);
        items.add(27);
        items.add(35);
        items.add(36);
        items.add(44);

        Icon background = getBackGround(7);
        for(int x = 0 ; x < 45; x++){
            if(!items.contains(x)) {
                inventoryCreator.setIcon(x, background);
            }
        }
    }

}

