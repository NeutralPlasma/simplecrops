package com.neutralplasma.simplecrops.gui.subGuis;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.gui.Handler;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.AbstractChatUtil;
import com.neutralplasma.simplecrops.utils.GuiItems;
import com.neutralplasma.simplecrops.utils.XMaterial;
import eu.virtusdevelops.virtuscore.gui.Icon;
import eu.virtusdevelops.virtuscore.gui.InventoryCreator;
import eu.virtusdevelops.virtuscore.gui.actions.*;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MessagesDropsGUI {
    private SimpleCrops simpleCrops;
    private MessagesHandler message;
    private Handler handler;


    private List<String> itemsList = new ArrayList<>();
    private List<Integer> items = new ArrayList<>();
    private HashMap<Integer,Integer> itemsMap = new HashMap<>();
    private InventoryCreator inventoryCreator;
    private int currentpage = 1;
    private String crop= "";

    public MessagesDropsGUI(SimpleCrops simpleCrops, MessagesHandler messagesHandler, Handler handler){
        this.handler = handler;
        this.simpleCrops = simpleCrops;
        this.message = messagesHandler;
        this.inventoryCreator = new InventoryCreator(45, TextUtil.colorFormat("&bEdit message drops."));
    }


    public void openGUI(Player player, String crop, int page){
        inventoryCreator.clean();
        items.clear();
        currentpage = page;
        this.crop = crop;
        setupDrops();
        prevPage();
        nextPage();
        createNew();
        createItemIcons();
        closeActionSetup();
        //setup background
        setupBackground();
        player.openInventory(inventoryCreator.getInventory());
    }

    public void closeActionSetup(){
        inventoryCreator.addCloseActions(new InventoryCloseAction() {
            @Override
            public void execute(Player player, Inventory inventory) {
                handler.openCropEditMenu(player, crop, true);
            }
        });
    }

    public void setupDrops(){

        itemsList.clear();
        itemsMap.clear();
        List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop + ".harvest.messages");
        //gets all item drops from config
        itemsList.addAll(drops);

        // Intializes pages for drops
        int page = 1;

        for(int x = 0; x < itemsList.size(); x++){
            if(x >= page*21){
                page++;
            }
            itemsMap.put(x, page);
        }
    }

    private void createItemIcons(){
        int itempos = 10;
        for(int id : itemsMap.keySet()) {
            if(itemsMap.get(id) == currentpage){
                String command = itemsList.get(id);
                ItemStack seedItem = new ItemStack(XMaterial.BOOK.parseMaterial());
                ItemMeta meta = seedItem.getItemMeta();

                List<String> lore = new ArrayList<>();
                lore.addAll(message.getList("gui.messageDropsGUI.message.lore"));
                String commandText = command;
                List<String> newList = new ArrayList<>();
                for(String line : lore){
                    if (command.contains("[player]")) {
                        line = line.replace("{1}", message.getMessage("gui.messageDropsGUI.message.player"));
                    }
                    if (command.contains("[console]")) {
                        line = line.replace("{1}", message.getMessage("gui.messageDropsGUI.message.console"));
                    }
                    if (command.contains("[broadcast]")) {
                        line = line.replace("{1}", message.getMessage("gui.messageDropsGUI.message.broadcast"));
                    }
                    commandText = commandText.replace("[player]", "");
                    commandText = commandText.replace("[console]", "");
                    commandText = commandText.replace("[broadcast]", "");
                    line = line.replace("{0}", commandText);
                    newList.add(line);
                }

                meta.setLore(newList);
                seedItem.setItemMeta(meta);
                Icon itemic = new Icon(seedItem);
                itemic.addLeftClickAction(new LeftClickAction() {
                    @Override
                    public void execute(Player player) {
                        List<String> messages = message.getList("gui.cropEditGui.messagesDrops.chatMessage");
                        for(String line : messages){
                            player.sendMessage(line);
                        }
                        handler.addToList(player.getUniqueId());
                        AbstractChatUtil chat = new AbstractChatUtil(player, event -> {
                            try {
                                if(!event.getMessage().equalsIgnoreCase(command)) {
                                    List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop + ".harvest.messages");
                                    drops.remove(command);
                                    drops.add(event.getMessage());
                                    simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + crop + ".harvest.messages", drops);
                                    simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                                    //simpleCrops.saveConfig();
                                }
                            } catch (Exception error){
                                player.sendMessage(TextUtil.colorFormat("&cError! Check console."));
                                error.printStackTrace();
                            }
                        }, simpleCrops);
                        chat.setOnClose(() -> {
                            handler.removeFromList(player.getUniqueId());
                            openGUI(player, crop, currentpage);
                        });
                    }
                });
                itemic.addRightClickAction(new RightClickAction() {
                    @Override
                    public void execute(Player player) {
                        List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop + ".harvest.messages");
                        drops.remove(command);
                        simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + crop + ".harvest.messages", drops);
                        simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                        //simpleCrops.saveConfig();
                        openGUI(player, crop, currentpage);
                    }
                });
                items.add(itempos);
                inventoryCreator.setIcon(itempos, itemic);

                if(itempos == 16){
                    itempos = 19;
                }else if(itempos == 25){
                    itempos = 28;
                }else {
                    itempos++;
                }
            }
        }

    }

    public void nextPage(){
        ItemStack nextPageItem = new ItemStack(XMaterial.PAPER.parseMaterial());
        ItemMeta itemMeta = nextPageItem.getItemMeta();
        itemMeta.setDisplayName(message.getMessage("gui.nextPage"));
        nextPageItem.setItemMeta(itemMeta);
        Icon nextpage = new Icon(nextPageItem);
        nextpage.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                currentpage++;
                openGUI(player, crop, currentpage);
            }
        });
        items.add(43);
        inventoryCreator.setIcon(43, nextpage);
    }

    public void prevPage() {
        if (currentpage > 1 ) {
            ItemStack prevPage = new ItemStack(XMaterial.PAPER.parseMaterial());
            ItemMeta itemMeta = prevPage.getItemMeta();
            itemMeta.setDisplayName(message.getMessage("gui.prevPage"));
            prevPage.setItemMeta(itemMeta);
            Icon nextpage = new Icon(prevPage);
            nextpage.addClickAction(new ClickAction() {
                @Override
                public void execute(Player player) {
                    currentpage--;
                    openGUI(player, crop, currentpage);
                }
            });
            items.add(37);
            inventoryCreator.setIcon(37, nextpage);
        }
    }

    public void createNew(){
        ItemStack book = new ItemStack(XMaterial.PAPER.parseMaterial());
        ItemMeta itemMeta = book.getItemMeta();
        itemMeta.setDisplayName(message.getMessage("gui.createNew"));
        book.setItemMeta(itemMeta);
        Icon addItem = new Icon(book);
        addItem.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                List<String> messages = message.getList("gui.cropEditGui.messagesDrops.chatMessage");
                for(String line : messages){
                    player.sendMessage(line);
                }
                handler.addToList(player.getUniqueId());
                AbstractChatUtil chat = new AbstractChatUtil(player, event -> {
                    try {
                        List<String> drops = simpleCrops.getFileManager().getConfiguration("configuration/crops").getStringList("crops." + crop + ".harvest.messages");
                        drops.add(event.getMessage());
                        simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + crop + ".harvest.messages", drops);
                        simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                        //simpleCrops.saveConfig();
                    } catch (Exception error){
                        player.sendMessage(TextUtil.colorFormat("&cError! Check console."));
                        error.printStackTrace();
                    }
                }, simpleCrops);
                chat.setOnClose(() -> {
                    handler.removeFromList(player.getUniqueId());
                    openGUI(player, crop, currentpage);
                });
            }
        });
        items.add(41);
        inventoryCreator.setIcon(41, addItem);
    }
    public Icon getBackGround(int glassid){
        ItemStack glass;
        glass = new ItemStack(GuiItems.getGlass(glassid));
        ItemMeta meta = glass.getItemMeta();
        meta.setDisplayName("");
        glass.setItemMeta(meta);
        Icon background = new Icon(glass);
        background.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
            }
        });
        return background;
    }

    public void setupBackground(){
        inventoryCreator.setIcon(0, getBackGround(2));
        inventoryCreator.setIcon(1, getBackGround(2));
        inventoryCreator.setIcon(7, getBackGround(2));
        inventoryCreator.setIcon(8, getBackGround(2));
        inventoryCreator.setIcon(9, getBackGround(2));
        inventoryCreator.setIcon(17, getBackGround(2));
        inventoryCreator.setIcon(27, getBackGround(2));
        inventoryCreator.setIcon(35, getBackGround(2));
        inventoryCreator.setIcon(36, getBackGround(2));
        inventoryCreator.setIcon(44, getBackGround(2));
        items.add(0);
        items.add(1);
        items.add(7);
        items.add(8);
        items.add(9);
        items.add(17);
        items.add(27);
        items.add(35);
        items.add(36);
        items.add(44);

        Icon background = getBackGround(7);
        for(int x = 0 ; x < 45; x++){
            if(!items.contains(x)) {
                inventoryCreator.setIcon(x, background);
            }
        }
    }
}
