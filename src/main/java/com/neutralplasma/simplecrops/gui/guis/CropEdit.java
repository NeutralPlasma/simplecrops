package com.neutralplasma.simplecrops.gui.guis;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.gui.Handler;
import eu.virtusdevelops.virtuscore.gui.Icon;
import eu.virtusdevelops.virtuscore.gui.InventoryCreator;
import eu.virtusdevelops.virtuscore.gui.actions.*;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.*;
import jdk.nashorn.internal.ir.Block;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class CropEdit {
    private SimpleCrops simpleCrops;
    private Handler handler;
    private MessagesHandler message;
    private ItemUtil itemUtil;
    private CropDrops cropDrops;

    private InventoryCreator inventoryCreator;
    private String cropname = "";
    private String firstName = "";
    List<Integer> items = new ArrayList<>();
    public CropEdit(SimpleCrops simpleCrops, Handler handler, MessagesHandler messagesHandler, ItemUtil itemUtil,
                    CropDrops cropDrops) {
        this.handler = handler;
        this.simpleCrops = simpleCrops;
        this.message = messagesHandler;
        this.itemUtil = itemUtil;
        this.cropDrops = cropDrops;

        inventoryCreator = new InventoryCreator(45, TextUtil.colorFormat("&bCrop edit."));
    }

    public void openGui(String cropname, Player player, boolean firstTime){
        inventoryCreator.clean();
        inventoryCreator.setTitle(TextUtil.colorFormat(message.getMessage("gui.cropEditGui.inventoryName").replace("{0}", cropname)));
        if(firstTime){
            firstName = cropname;
            this.cropname = cropname;
        }
        closeActionSetup();
        // setup inv items.
        createAnvilName();
        createNameTagItem();
        commandDrops();
        messageDrops();
        //itemDrops();
        customItemDrops();
        createAnvilStrenghtGain();
        seedTypeItem();
        // Background
        setupBackground();
        // open inv.
        player.openInventory(inventoryCreator.getInventory());
    }

    public void closeActionSetup(){
        inventoryCreator.addCloseActions(new InventoryCloseAction() {
            @Override
            public void execute(Player player, Inventory inventory) {
                handler.openCropsMenu(1, player);
            }
        });
    }

    public void createNameTagItem(){
        ItemStack nametag = new ItemStack(Material.NAME_TAG);
        ItemMeta namemeta = nametag.getItemMeta();
        namemeta.setDisplayName(TextUtil.colorFormat(message.getMessage("gui.cropEditGui.cropName.itemName").replace("{0}", cropname)));
        List<String> lore;
        lore = message.getRawList("gui.cropEditGui.cropName.lore");
        namemeta.setLore(TextUtil.colorFormatList(lore));
        nametag.setItemMeta(namemeta);
        Icon icon = new Icon(nametag);
        icon.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                AbstractAnvilGUI gui = new AbstractAnvilGUI(player, event -> {
                    try {
                        cropname = event.getName();
                        ConfigurationSection section = simpleCrops.getFileManager().getConfiguration("configuration/crops").getConfigurationSection("crops." + firstName);
                        if(!firstName.equalsIgnoreCase(cropname)) {
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + firstName, null);
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + cropname, section);
                            simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                            firstName = cropname;
                        }
                    } catch (Exception ex) {
                        player.sendMessage("Error.");
                    }

                }, simpleCrops);
                ItemStack itemp = new ItemStack(Material.PAPER);
                ItemMeta meta = itemp.getItemMeta();
                meta.setDisplayName("Example: Coal");
                itemp.setItemMeta(meta);

                gui.setSlot(AbstractAnvilGUI.AnvilSlot.INPUT_LEFT, itemp);
                gui.setOnClose((player1, inventory1) -> {
                    openGui(cropname, player1, false);
                });
                gui.open();
            }
        });
        items.add(16);
        inventoryCreator.setIcon(16, icon);
    }

    public void createAnvilName(){
        ItemStack anvil = new ItemStack(Material.BOOK);
        ItemMeta anvilmeta = anvil.getItemMeta();
        anvilmeta.setDisplayName(TextUtil.colorFormat(message.getMessage("gui.cropEditGui.cropName.itemName").replace("{0}", simpleCrops.getFileManager().getConfiguration("configuration/crops").getString("crops." + firstName + ".name"))));
        List<String> lore;
        lore = message.getRawList("gui.cropEditGui.cropDisplayName.lore");
        anvilmeta.setLore(TextUtil.colorFormatList(lore));
        anvil.setItemMeta(anvilmeta);
        Icon icon = new Icon(anvil);
        icon.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                AbstractAnvilGUI gui = new AbstractAnvilGUI(player, event -> {
                    try {
                        simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + firstName + ".name", event.getName());
                        simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                    } catch (Exception ex) {
                        player.sendMessage("Error!");
                    }
                }, simpleCrops);
                ItemStack itemp = new ItemStack(Material.PAPER);
                ItemMeta meta = itemp.getItemMeta();
                meta.setDisplayName("Example: &eCoal");
                itemp.setItemMeta(meta);

                gui.setSlot(AbstractAnvilGUI.AnvilSlot.INPUT_LEFT, itemp);
                gui.setOnClose((player1, inventory1) -> {
                    openGui(cropname, player1, false);
                });
                gui.open();
            }
        });
        items.add(31);
        inventoryCreator.setIcon(31, icon);
    }

    public void commandDrops(){
        ItemStack book = new ItemStack(XMaterial.COMMAND_BLOCK.parseMaterial());
        ItemMeta meta = book.getItemMeta();
        meta.setDisplayName(message.getMessage("gui.cropEditGui.commandDrops.itemName"));
        List<String> lore;
        lore = message.getRawList("gui.cropEditGui.commandDrops.lore");
        meta.setLore(TextUtil.colorFormatList(lore));
        book.setItemMeta(meta);
        Icon icon = new Icon(book);
        icon.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                handler.openCommandsMenu(player, cropname, 1);
            }
        });
        items.add(10);
        inventoryCreator.setIcon(10, icon);
    }

    public void seedTypeItem(){
        ItemStack seed;
        try{
            seed = itemUtil.decodeItem(simpleCrops.getFileManager().getConfiguration("configuration/crops").getString("crops." + firstName + ".seed-type"))
            /*Material.getMaterial(simpleCrops.getConfig().getString("seeds." + firstName + ".seed-type"))*/;

            //seed = new ItemStack(mat);
        }catch (Exception error){
            seed = new ItemStack(XMaterial.WHEAT_SEEDS.parseMaterial(), 1);
        }
        ItemMeta anvilmeta = seed.getItemMeta();
        anvilmeta.setDisplayName(null);
        anvilmeta.setDisplayName(TextUtil.colorFormat(message.getRawMessage("gui.cropEditGui.cropTypeItem.name").replace("{0}", cropDrops.getSeed(firstName).getType().toString())));

        //List<String> lore;
        //lore = message.getRawList("gui.cropEditGui.cropTypeItem.lore");
        //anvilmeta.setLore(TextUtil.colorFormatList(lore));

        seed.setItemMeta(anvilmeta);
        Icon icon = new Icon(seed);
        icon.addDragItemIntoAction(new DragItemIntoAction() {
            @Override
            public void execute(Player player, ItemStack item) {
                if(!item.getType().toString().equalsIgnoreCase("AIR")) {
                    String itemData = itemUtil.encodeItem(item);

                    simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + firstName + ".seed-type", "custom:" + itemData);
                    simpleCrops.getFileManager().saveFile("configuration/crops.yml");

                    //simpleCrops.getConfig().set("seeds." + firstName + ".seed-type", itemData);
                    //simpleCrops.saveConfig();
                    openGui(cropname, player, false);
                }else{
                    player.sendMessage(message.getMessage("gui.cropEditGui.cropTypeItem.invalidItem"));
                }
            }
        });
        items.add(23);
        inventoryCreator.setIcon(23, icon);
    }

    public void createAnvilStrenghtGain(){
        ItemStack item = new ItemStack(XMaterial.ANVIL.parseMaterial());
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(TextUtil.colorFormat(message.getRawMessage("gui.cropEditGui.gainAndStrenghtItem.name")));
        ConfigurationSection section = simpleCrops.getFileManager().getConfiguration("configuration/crops").getConfigurationSection("crops." + firstName);
        int minGain = 0, maxGain = 0, maxStrength = 0, minStrength = 0;
        try {
            minGain = section.getInt("gain.min");
            maxGain = section.getInt("gain.max");
        }catch (Exception error){
            minGain = -1;
            maxGain = -1;
        }
        try{
            minStrength = section.getInt("strength.min");
            maxStrength = section.getInt("strength.max");
        }catch (Exception error){
            minStrength = -1;
            maxStrength = -1;
        }

        List<String> lore, newlore = new ArrayList<>();

        lore = message.getRawList("gui.cropEditGui.gainAndStrenghtItem.lore");
        for(String line : lore){
            line = line.replace("{0}", String.valueOf(minGain));
            line = line.replace("{1}", String.valueOf(maxGain));
            line = line.replace("{2}", String.valueOf(minStrength));
            line = line.replace("{3}", String.valueOf(maxStrength));
            newlore.add(TextUtil.colorFormat(line));
        }
        itemMeta.setLore(TextUtil.colorFormatList(newlore));
        item.setItemMeta(itemMeta);
        Icon icon = new Icon(item);
        icon.addRightClickAction(new RightClickAction() {
            @Override
            public void execute(Player player) {
                AbstractAnvilGUI gui = new AbstractAnvilGUI(player, event -> {
                    try {
                        String[] entered = event.getName().split(":");
                        ConfigurationSection section = simpleCrops.getFileManager().getConfiguration("configuration/crops").getConfigurationSection("seeds." + firstName + ".gain");
                        int minGain = 0, maxGain = 0;
                        try {
                            minGain = section.getInt("min");
                            maxGain = section.getInt("max");

                        }catch (Exception e){
                            player.sendMessage("Error!");
                            openGui(cropname, player, false);
                        }
                        if(minGain != Integer.valueOf(entered[0]) && maxGain != Integer.valueOf(entered[1])) {
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + cropname + ".gain.max", Integer.valueOf(entered[1]));
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + cropname + ".gain.min", Integer.valueOf(entered[0]));
                            simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                        }
                    } catch (Exception ex) {
                        player.sendMessage("Error.");
                    }

                }, simpleCrops);
                ItemStack itemp = new ItemStack(XMaterial.PAPER.parseMaterial());
                ItemMeta meta = itemp.getItemMeta();
                meta.setDisplayName("<MIN:MAX>");
                itemp.setItemMeta(meta);

                gui.setSlot(AbstractAnvilGUI.AnvilSlot.INPUT_LEFT, itemp);
                gui.setOnClose((player1, inventory1) -> {
                    openGui(cropname, player1, false);
                });
                gui.open();
            }
        });

        icon.addLeftClickAction(new LeftClickAction() {
            @Override
            public void execute(Player player) {
                AbstractAnvilGUI gui = new AbstractAnvilGUI(player, event -> {
                    try {
                        String[] entered = event.getName().split(":");
                        simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + cropname + ".strength.max", Integer.valueOf(entered[1]));
                        simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + cropname + ".strength.min", Integer.valueOf(entered[0]));
                        simpleCrops.getFileManager().saveFile("configuration/crops.yml");
                    } catch (Exception ex) {
                        player.sendMessage("Error.");
                    }

                }, simpleCrops);
                ItemStack itemp = new ItemStack(XMaterial.PAPER.parseMaterial());
                ItemMeta meta = itemp.getItemMeta();
                meta.setDisplayName("<MIN:MAX>");
                itemp.setItemMeta(meta);

                gui.setSlot(AbstractAnvilGUI.AnvilSlot.INPUT_LEFT, itemp);
                gui.setOnClose((player1, inventory1) -> {
                    openGui(cropname, player1, false);
                });
                gui.open();

            }
        });
        items.add(21);
        inventoryCreator.setIcon(21, icon);
    }

    public void messageDrops(){
        ItemStack paper = new ItemStack(XMaterial.PAPER.parseMaterial());
        ItemMeta meta = paper.getItemMeta();
        meta.setDisplayName(message.getMessage("gui.cropEditGui.messagesDrops.itemName"));
        List<String> lore;
        lore = message.getRawList("gui.cropEditGui.messagesDrops.lore");
        meta.setLore(TextUtil.colorFormatList(lore));
        paper.setItemMeta(meta);
        Icon icon = new Icon(paper);
        icon.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                handler.openMessagesMenu(player, cropname, 1);
            }
        });
        items.add(13);
        inventoryCreator.setIcon(13, icon);
    }

    /*public void itemDrops(){
        ItemStack chest = new ItemStack(XMaterial.CHEST.parseMaterial());
        ItemMeta meta = chest.getItemMeta();
        meta.setDisplayName(message.getMessage("gui.cropEditGui.itemDrops.itemName"));
        List<String> lore;
        lore = message.getRawList("gui.cropEditGui.itemDrops.lore");
        meta.setLore(TextUtil.colorFormatList(lore));
        chest.setItemMeta(meta);
        Icon icon = new Icon(chest);
        icon.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                handler.openItemsMenu(player, cropname, 1);
            }
        });
        items.add(29);
        inventoryCreator.setIcon(29, icon);
    }*/
    public void customItemDrops(){
        ItemStack shulker = new ItemStack(XMaterial.CHEST.parseMaterial());
        ItemMeta meta = shulker.getItemMeta();
        meta.setDisplayName(message.getMessage("gui.cropEditGui.customItemDrops.itemName"));
        List<String> lore;
        lore = message.getRawList("gui.cropEditGui.customItemDrops.lore");
        meta.setLore(TextUtil.colorFormatList(lore));
        shulker.setItemMeta(meta);
        Icon icon = new Icon(shulker);
        icon.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                handler.openCustomDropsMenu(player, cropname, 1);
            }
        });
        items.add(33);
        inventoryCreator.setIcon(33, icon);
    }

    public Icon getBackGround(int glassid){
        ItemStack glass;
        glass = new ItemStack(GuiItems.getGlass(glassid));
        ItemMeta meta = glass.getItemMeta();
        meta.setDisplayName("");
        glass.setItemMeta(meta);
        Icon background = new Icon(glass);
        background.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
            }
        });
        return background;
    }

    public void setupBackground(){
        inventoryCreator.setIcon(0, getBackGround(2));
        inventoryCreator.setIcon(1, getBackGround(2));
        inventoryCreator.setIcon(7, getBackGround(2));
        inventoryCreator.setIcon(8, getBackGround(2));
        inventoryCreator.setIcon(9, getBackGround(2));
        inventoryCreator.setIcon(17, getBackGround(2));
        inventoryCreator.setIcon(27, getBackGround(2));
        inventoryCreator.setIcon(35, getBackGround(2));
        inventoryCreator.setIcon(36, getBackGround(2));
        inventoryCreator.setIcon(44, getBackGround(2));
        items.add(0);
        items.add(1);
        items.add(7);
        items.add(8);
        items.add(9);
        items.add(17);
        items.add(27);
        items.add(35);
        items.add(36);
        items.add(44);

        Icon background = getBackGround(7);
        for(int x = 0 ; x < 45; x++){
            if(!items.contains(x)) {
                inventoryCreator.setIcon(x, background);
            }
        }
    }
}
