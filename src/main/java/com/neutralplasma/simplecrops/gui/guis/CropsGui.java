package com.neutralplasma.simplecrops.gui.guis;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.gui.Handler;
import eu.virtusdevelops.virtuscore.gui.Icon;
import eu.virtusdevelops.virtuscore.gui.InventoryCreator;
import eu.virtusdevelops.virtuscore.gui.actions.*;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CropsGui {
    private Handler handler;
    private SimpleCrops simpleCrops;
    private MessagesHandler message;
    private ItemUtil itemUtil;
    private CropDrops cropDrops;

    private InventoryCreator inventoryCreator;
    private String cropName = "";
    boolean exists = true;
    private List<String> cropsList = new ArrayList<>();
    private HashMap<String,Integer> cropsMap = new HashMap<>();

    List<Integer> items = new ArrayList<>();

    private int currentpage = 1;

    public CropsGui(SimpleCrops simpleCrops, MessagesHandler message, Handler handler, ItemUtil itemUtil, CropDrops cropDrops){
        this.message = message;
        this.simpleCrops = simpleCrops;
        this.handler = handler;
        this.itemUtil = itemUtil;
        this.cropDrops = cropDrops;
        this.inventoryCreator = new InventoryCreator(45, message.getMessage("gui.CropsGui.inventoryName"));
        setupCrops();
    }

    public void setupCrops(){
        cropsList.clear();
        cropsMap.clear();
        ConfigurationSection configurationSection = simpleCrops.getFileManager().getConfiguration("configuration/crops").getConfigurationSection("crops");

        //gets all seeds from config
        for(String entered : configurationSection.getKeys(true)){
            if(!entered.contains(".")){
                cropsList.add(entered);
            }
        }
        // Intializes pages for seeds
        int page = 1;

        for(int x = 0; x < cropsList.size(); x++){
            if(x >= page*21){
                page++;
            }
            cropsMap.put(cropsList.get(x), page);
        }

    }

    public void openGui(int page, Player player){
        inventoryCreator.clean();
        items.clear();

        inventoryCreator.setTitle(message.getMessage("gui.CropsGui.inventoryName"));
        setupCrops();
        currentpage = page;
        createSeedIcons(page);
        nextPage();
        prevPage();
        createNew();
        setupBackground();

        player.openInventory(inventoryCreator.getInventory());
    }


    public void createSeedIcons(int page){
        int itempos = 10;
        for(String item : cropsMap.keySet()){
            if(cropsMap.get(item) == page){
                ItemStack seedItem;

                seedItem = cropDrops.getSeed(item);
                //String name = TextUtil.colorFormat(simpleCrops.getFileManager().getConfiguration("configuration/crops").getString("crops." + item + ".name"));
                ItemMeta meta = seedItem.getItemMeta();
                meta.setDisplayName(item);
                seedItem.setItemMeta(meta);

                Icon seed = new Icon(seedItem);
                seed.addRightClickAction(new RightClickAction() {
                    @Override
                    public void execute(Player player) {
                        simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + item, null);

                        simpleCrops.getFileManager().saveFile("configuration/crops.yml");

                        handler.openCropsMenu(currentpage, player);
                    }
                });
                seed.addLeftClickAction(new LeftClickAction() {
                    @Override
                    public void execute(Player player) {
                        handler.openCropEditMenu(player, item, true);
                    }
                });
                items.add(itempos);
                inventoryCreator.setIcon(itempos, seed);

                if(itempos == 16){
                    itempos = 19;
                }else if(itempos == 25){
                    itempos = 28;
                }else {
                    itempos++;
                }
            }
        }
    }


    public void nextPage(){
        ItemStack nextPageItem = new ItemStack(XMaterial.PAPER.parseMaterial());
        ItemMeta itemMeta = nextPageItem.getItemMeta();
        itemMeta.setDisplayName(message.getMessage("gui.nextPage"));
        nextPageItem.setItemMeta(itemMeta);
        Icon nextpage = new Icon(nextPageItem);
        nextpage.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                currentpage++;
                openGui(currentpage, player);
            }
        });
        items.add(43);
        inventoryCreator.setIcon(43, nextpage);
    }

    public void prevPage() {
        if (currentpage > 1 ) {
            ItemStack prevPage = new ItemStack(XMaterial.PAPER.parseMaterial());
            ItemMeta itemMeta = prevPage.getItemMeta();
            itemMeta.setDisplayName(message.getMessage("gui.prevPage"));
            prevPage.setItemMeta(itemMeta);
            Icon nextpage = new Icon(prevPage);
            nextpage.addClickAction(new ClickAction() {
                @Override
                public void execute(Player player) {
                    currentpage--;
                    openGui(currentpage, player);
                }
            });
            items.add(37);
            inventoryCreator.setIcon(37, nextpage);
        }
    }

    public void createNew(){
        ItemStack book = new ItemStack(XMaterial.BOOK.parseMaterial());
        ItemMeta itemMeta = book.getItemMeta();
        itemMeta.setDisplayName(message.getMessage("gui.createNew"));
        book.setItemMeta(itemMeta);
        Icon addItem = new Icon(book);
        addItem.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                AbstractAnvilGUI gui = new AbstractAnvilGUI(player, event -> {
                    try {
                        if(!simpleCrops.getFileManager().getConfiguration("configuration/crops").contains("crops." + event.getName())){
                            List<String> items = new ArrayList<>();
                            List<String> commands = new ArrayList<>();
                            List<String> messages = new ArrayList<>();

                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".name", "&eExampleName");
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".strength.min", 0);
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".strength.max", 10);
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".gain.min", 0);
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".gain.max", 10);
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".seed-type", "custom:" + itemUtil.encodeItem(XMaterial.WHEAT_SEEDS.parseItem()));
                            items.add("default:STONE:1:2");
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".harvest.items", items);
                            commands.add("eco give %p 100");
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".harvest.commands", commands);
                            messages.add("[player]&7You've broken &c%cropname&7.");
                            messages.add("[broadcast]&c%p&7 broken &c%cropname&7.");
                            messages.add("[console]&8[&bLog&8] &c%p &7broken &c%cropname&7.");
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".harvest.messages", messages);
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".bonemeal.useCustom", false);
                            simpleCrops.getFileManager().getConfiguration("configuration/crops").set("crops." + event.getName() + ".bonemeal.number", 5);

                            simpleCrops.getFileManager().saveFile("configuration/crops.yml");

                        }else{
                            player.sendMessage(message.getMessage("gui.CropsGui.alreadyExists"));
                            player.closeInventory();
                        }

                    } catch (Exception error){
                        player.sendMessage(TextUtil.colorFormat("&cError! Check console."));
                        error.printStackTrace();
                    }
                }, simpleCrops);
                ItemStack itemp = new ItemStack(XMaterial.PAPER.parseMaterial());
                ItemMeta meta = itemp.getItemMeta();
                meta.setDisplayName("<CROPNAME>");
                itemp.setItemMeta(meta);
                gui.setSlot(AbstractAnvilGUI.AnvilSlot.INPUT_LEFT, itemp);
                gui.setOnClose((player1, inventory1) -> {
                    if(exists) {
                        openGui(currentpage, player1);
                    }
                });
                gui.open();
            }
        });
        items.add(41);
        inventoryCreator.setIcon(41, addItem);
    }


    public Icon getBackGround(int glassid){
        ItemStack glass;
        glass = new ItemStack(GuiItems.getGlass(glassid));
        ItemMeta meta = glass.getItemMeta();
        meta.setDisplayName("");
        glass.setItemMeta(meta);
        Icon background = new Icon(glass);
        background.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
            }
        });
        return background;
    }

    public void setupBackground(){
        inventoryCreator.setIcon(0, getBackGround(2));
        inventoryCreator.setIcon(1, getBackGround(2));
        inventoryCreator.setIcon(7, getBackGround(2));
        inventoryCreator.setIcon(8, getBackGround(2));
        inventoryCreator.setIcon(9, getBackGround(2));
        inventoryCreator.setIcon(17, getBackGround(2));
        inventoryCreator.setIcon(27, getBackGround(2));
        inventoryCreator.setIcon(35, getBackGround(2));
        inventoryCreator.setIcon(36, getBackGround(2));
        inventoryCreator.setIcon(44, getBackGround(2));
        items.add(0);
        items.add(1);
        items.add(7);
        items.add(8);
        items.add(9);
        items.add(17);
        items.add(27);
        items.add(35);
        items.add(36);
        items.add(44);

        Icon background = getBackGround(7);
        for(int x = 0 ; x < 45; x++){
            if(!items.contains(x)) {
                inventoryCreator.setIcon(x, background);
            }
        }
    }
}
