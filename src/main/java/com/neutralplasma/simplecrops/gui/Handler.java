package com.neutralplasma.simplecrops.gui;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.gui.guis.CropEdit;
import com.neutralplasma.simplecrops.gui.guis.CropsGui;
import com.neutralplasma.simplecrops.gui.subGuis.CommandDropsGUI;
import com.neutralplasma.simplecrops.gui.subGuis.CustomDropsGUI;
import com.neutralplasma.simplecrops.gui.subGuis.MessagesDropsGUI;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.CropDrops;
import com.neutralplasma.simplecrops.utils.ItemUtil;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Handler {

    private SimpleCrops simpleCrops;
    private ItemUtil itemUtil;
    private CommandDropsGUI commandDropsGUI;
    private CustomDropsGUI customDropsGUI;
    private MessagesDropsGUI messagesDropsGUI;
    private CropEdit cropEdit;
    private CropsGui cropsGui;

    private List<UUID> openedInv = new ArrayList<>();

    public Handler(SimpleCrops simpleCrops, MessagesHandler messagesHandler, ItemUtil itemUtil, CropDrops cropDrops){
        this.simpleCrops = simpleCrops;
        this.itemUtil = itemUtil;
        commandDropsGUI = new CommandDropsGUI(simpleCrops, messagesHandler, this);
        customDropsGUI = new CustomDropsGUI(simpleCrops, this, itemUtil, messagesHandler);
        messagesDropsGUI = new MessagesDropsGUI(simpleCrops, messagesHandler, this);
        cropEdit = new CropEdit(simpleCrops, this, messagesHandler, itemUtil, cropDrops);
        cropsGui = new CropsGui(simpleCrops, messagesHandler, this, itemUtil, cropDrops);
    }

    public void openMessagesMenu(Player player, String crop, int page){
        messagesDropsGUI.openGUI(player, crop, page);
    }

    public void openCommandsMenu(Player player, String crop, int page){
        commandDropsGUI.openGUI(player, crop, page);
    }

    public void openCustomDropsMenu(Player player, String crop, int page){
        customDropsGUI.openGUI(player, crop, page);
    }


    public void openCropEditMenu(Player player, String crop, boolean firstTime){
        cropEdit.openGui(crop, player, firstTime);
    }

    public void openCropsMenu(int page, Player player){
        cropsGui.openGui(page, player);
    }

    public void addToList(UUID uuid){
        openedInv.add(uuid);
    }
    public void removeFromList(UUID uuid){
        openedInv.remove(uuid);
    }
    public boolean hasOpened(UUID uuid) {
        return openedInv.contains(uuid);
    }
}
