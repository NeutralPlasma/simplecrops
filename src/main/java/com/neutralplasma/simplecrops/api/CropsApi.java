package com.neutralplasma.simplecrops.api;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.utils.Crop;
import com.neutralplasma.simplecrops.utils.CropDrops;
import com.neutralplasma.simplecrops.utils.CropLocation;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class CropsApi {
    private static SimpleCrops simpleCrops;
    private static DataManager dataManager;
    private static CropDrops cropDrops;

    public CropsApi(SimpleCrops simpleCrops, DataManager dataManager, CropDrops cropDrops){
        this.simpleCrops = simpleCrops;
        this.dataManager = dataManager;
        this.cropDrops = cropDrops;
    }


    public static boolean isCustomCrop(Block crop){
        CropLocation cropLocation = new CropLocation(crop.getLocation().getWorld().toString(), crop.getX(), crop.getY(), crop.getZ()); // Gets the crop location
        Crop cCrop = dataManager.getCrop(cropLocation); // Gets the crop from database if its saved.
        if(cCrop != null){
            return true;
        }
        return false;

    }

    public static Crop getCrop(Block block){
        CropLocation cropLocation = new CropLocation(block.getLocation().getWorld().toString(), block.getX(), block.getY(), block.getZ()); // Gets the crop location
        Crop cCrop = dataManager.getCrop(cropLocation); // Gets the crop from database if its saved.
        if(cCrop != null){
            return cCrop;
        }
        return null;
    }

    public static List<ItemStack> getCropDrops(Crop crop){
        return cropDrops.getDrops(crop);
    }

    public static boolean removeCrop(CropLocation cropLocation, Crop crop){
        return dataManager.removeCrop(cropLocation, crop);
    }

    public static boolean saveCrop(CropLocation cropLocation, Crop crop){
        return dataManager.addPlant(crop, cropLocation);
    }

}
