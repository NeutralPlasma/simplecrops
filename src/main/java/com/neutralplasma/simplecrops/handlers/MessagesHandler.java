package com.neutralplasma.simplecrops.handlers;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.utils.Crop;
import com.neutralplasma.simplecrops.utils.CropLocation;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;


public class MessagesHandler {
    private MessagesData messagesData;
    private SimpleCrops simpleCrops;

    public MessagesHandler(MessagesData messagesData, SimpleCrops simpleCrops){
        this.simpleCrops = simpleCrops;
        this.messagesData = messagesData;
    }

    public String getMessage(String path){
        String message = messagesData.getMessages().getString(path);
        if(message == null){
            return path;
        }
        return TextUtil.colorFormat(message);
    }

    public String getRawMessage(String path){
        return messagesData.getMessages().getString(path);
    }

    public List<String> getList(String path){
        return TextUtil.colorFormatList(getRawList(path));
    }

    public List<String> formatList(List<String> list, String replace, String value){
        List<String> newList = new ArrayList<>();
        for(String st : list){
            st = st.replace(replace, value);
            newList.add(TextUtil.colorFormat(st));
        }
        return newList;
    }

    public List<String> formatList(List<String> list, String[] replaces, String[] values){
        List<String> newList = new ArrayList<>();
        for(String st : list){
            for(int i = 0; i < replaces.length; i++){
                st = st.replace(replaces[i], values[i]);
            }
            newList.add(TextUtil.colorFormat(st));
        }
        return newList;
    }

    public List<String> getRawList(String path){
        List<String> list = messagesData.getMessages().getStringList(path);
        if(list.isEmpty()){
            list.add(path);
        }
        return list;
    }

    public void debug(Player player, Block block, String debugType){
        if(simpleCrops.getConfig().getBoolean("debug")) {
            TextComponent textComponent = new TextComponent();
            String name = player != null ? player.getName() : "ERROR!";
            ComponentBuilder component = new ComponentBuilder(TextUtil.colorFormat(
                    "&7Action: &e" + debugType.toUpperCase()
                            + " \n&7Block: &e" + block
                            + "\n&7BlockType: &e" + block.getType()
                            + "\n&7Location: X=&e" + block.getX() + " &7Y=&e" + block.getY() + " &7Z=&e" + block.getZ()
                            + "\n&7World: &e" + block.getWorld()
                            + "\n&7Player: &e" + name
            ));

            textComponent.setText(TextUtil.colorFormat("&8[&6Debug&8] &7Action: &e" + debugType.toUpperCase() + "&7. Block Type &8(&e" + block.getType() + "&8)&7."));
            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, component.create()));

            for (Player oplayer : Bukkit.getOnlinePlayers()) {
                if (oplayer.hasPermission("simplecrops.debug")) {
                    oplayer.spigot().sendMessage(textComponent);
                }
            }
        }
    }

    public void debug(Player player, CropLocation cropLocation, Crop crop, String debugType){
        if(simpleCrops.getConfig().getBoolean("debug")){
            TextComponent textComponent = new TextComponent();
            ComponentBuilder component;
            if (player != null) {
                component = new ComponentBuilder(TextUtil.colorFormat("&7Action: &e" + debugType.toUpperCase() + " \n&7Crop: &e" + crop.getName()
                        + "\n&7CropID: &e" + crop.getid()
                        + "\n&7Location: X=&e" + cropLocation.getX() + " &7Y=&e" + cropLocation.getY() + " &7Z=&e" + cropLocation.getZ()
                        + "\n&7World: &e" + cropLocation.getWorld()
                        + "\n&7Player: &e" + player.getName()));
            }else{
                component = new ComponentBuilder(TextUtil.colorFormat("&7Action: &e" + debugType.toUpperCase() + " \n&7Crop: &e" + crop.getName()
                        + "\n&7CropID: &e" + crop.getid()
                        + "\n&7Location: X=&e" + cropLocation.getX() + " &7Y=&e" + cropLocation.getY() + " &7Z=&e" + cropLocation.getZ()
                        + "\n&7World: &e" + cropLocation.getWorld()
                        + "\n&7Player: &e" + "NULL"));
            }


            textComponent.setText(TextUtil.colorFormat("&8[&6Debug&8] &7Action: &e" + debugType.toUpperCase() + "&7. Crop name &8(&e" + crop.getName() + "&8)&7."));
            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, component.create()));

            for(Player oplayer: Bukkit.getOnlinePlayers()){
                if(oplayer.hasPermission("simplecrops.debug")){
                    oplayer.spigot().sendMessage(textComponent);
                }
            }
        }

    }

}
