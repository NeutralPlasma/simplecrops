package com.neutralplasma.simplecrops.handlers;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.events.crop.LevelUpData;
import com.neutralplasma.simplecrops.utils.Crop;
import eu.virtusdevelops.virtuscore.managers.FileManager;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class CropLeveling {
    private SimpleCrops simpleCrops;
    private FileManager fileManager;

    public CropLeveling(SimpleCrops simpleCrops){
        this.simpleCrops = simpleCrops;
        this.fileManager = simpleCrops.getFileManager();
    }



    public Crop levelUpCrop(Crop crop){
        int gain = crop.getGain();
        int strength = crop.getStrength();
        String cropid = crop.getid();
        int maxgain = fileManager.getConfiguration("configuration/crops").getInt("crops." + cropid + ".gain.max");
        int maxstrength = fileManager.getConfiguration("configuration/crops").getInt("crops." + cropid + ".strength.max");
        int minstrength = fileManager.getConfiguration("configuration/crops").getInt("crops." + cropid + ".strength.min");

        boolean levelUpStrength = shouldLevelUp(minstrength, maxstrength, strength);
        if(levelUpStrength){
            int newStrength = strength+1;
            if(newStrength > maxstrength){
                newStrength = maxstrength;
            }
            crop.setStrength(newStrength);
        }

        if(shouldLevelUp(minstrength, maxstrength, strength)){
            int newGain = gain+1;
            if(newGain > maxgain){
                newGain = maxgain;
            }
            crop.setGain(newGain);
        }

        return crop;
    }

    public LevelUpData levelUpCrop(Crop crop, String ignored){
        int gain = crop.getGain();
        int strength = crop.getStrength();
        String cropid = crop.getid();
        int maxgain = fileManager.getConfiguration("configuration/crops").getInt("crops." + cropid + ".gain.max");
        int maxstrength = fileManager.getConfiguration("configuration/crops").getInt("crops." + cropid + ".strength.max");
        int minstrength = fileManager.getConfiguration("configuration/crops").getInt("crops." + cropid + ".strength.min");

        boolean levelUpStrength = shouldLevelUp(minstrength, maxstrength, strength);
        boolean didlevelup = false;
        if(levelUpStrength){
            int newStrength = strength+1;
            if(newStrength > maxstrength){
                newStrength = maxstrength;
            }else {
                didlevelup = true;
            }
            crop.setStrength(newStrength);
        }

        if(shouldLevelUp(minstrength, maxstrength, strength)){
            int newGain = gain+1;
            if(newGain > maxgain){
                newGain = maxgain;
            }else {
                didlevelup = true;
            }
            crop.setGain(newGain);
        }
        return new LevelUpData(crop, didlevelup);
    }

    public boolean shouldLevelUp(int min, int max, int current){
        int chance = new Random().nextInt((max - min) + 1);
        if (chance <= current) {
            return true;
        }
        return false;
    }
}
