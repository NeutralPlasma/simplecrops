package com.neutralplasma.simplecrops;

import com.neutralplasma.simplecrops.api.CropsApi;
import com.neutralplasma.simplecrops.commands.CommandHandler;
import com.neutralplasma.simplecrops.commands.MainCommand;
import com.neutralplasma.simplecrops.commands.TabComplete;
import com.neutralplasma.simplecrops.commands.subCommands.*;
import com.neutralplasma.simplecrops.datamanagers.DataManager;
import com.neutralplasma.simplecrops.datamanagers.MessagesData;
import com.neutralplasma.simplecrops.datamanagers.storagetypes.MySQL;
import com.neutralplasma.simplecrops.datamanagers.storagetypes.SQL;
import com.neutralplasma.simplecrops.datamanagers.storagetypes.Yaml;
import com.neutralplasma.simplecrops.events.*;
import com.neutralplasma.simplecrops.events.crop.*;
import com.neutralplasma.simplecrops.gui.Handler;
import com.neutralplasma.simplecrops.handlers.CropLeveling;
import com.neutralplasma.simplecrops.handlers.MessagesHandler;
import com.neutralplasma.simplecrops.utils.*;
import com.neutralplasma.simplecrops.utils.NBT.Current;
import com.neutralplasma.simplecrops.utils.NBT.Legacy;
import eu.virtusdevelops.virtuscore.VirtusCore;
import eu.virtusdevelops.virtuscore.managers.FileManager;
import eu.virtusdevelops.virtuscore.utils.FileLocation;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.util.Arrays;
import java.util.LinkedHashSet;


public class SimpleCrops extends JavaPlugin {

    private NBTDataUtil nbtDataUtil;
    private ItemUtil itemUtil;
    private CropDrops cropDrops;
    private MySQL mySQL;
    private SQL sql;
    private Yaml yaml;
    private DataManager dataManager;
    private MessagesData messagesData;
    private CropLeveling cropLeveling;
    private CropsApi cropsApi;
    private Handler handler;
    private MessagesHandler messagesHandler;
    private Legacy legacy;
    private Current current;
    private String version = "legacy";
    private MultiCropHandler multiCropHandler;
    private FileManager fileManager;

    @Override
    public void onEnable() {
        ConsoleCommandSender console = Bukkit.getConsoleSender();
        String line = "&a=================================";
        String messageline1 = "&6Made by Virtus Develops";
        String messageline2 = "&7SimpleCrops: &6" + this.getDescription().getVersion();
        String messageline3 = "&7Running on: " + getServer().getVersion();
        String messageline4 = "&7Action: &aEnabling plugin&7...";
        console.sendMessage(TextUtil.colorFormat(line));
        console.sendMessage(TextUtil.colorFormat(messageline1));
        console.sendMessage(TextUtil.colorFormat(messageline2));
        console.sendMessage(TextUtil.colorFormat(messageline3));
        console.sendMessage(TextUtil.colorFormat(messageline4));

        // VERSION SETUP
        if(Bukkit.getVersion().contains("1.13")){
            version = "legacy";
        }else if(Bukkit.getVersion().contains("1.14")){
            version = "middle";
        }else if(Bukkit.getVersion().contains("1.15")){
            version = "current";
        }

        messagesData = new MessagesData(this);
        messagesHandler = new MessagesHandler(messagesData, this);

        setupConfig(); // setups config.

        this.fileManager = new FileManager(this, new LinkedHashSet<>(Arrays.asList(
                FileLocation.of("configuration/crops.yml", true, false),
                FileLocation.of("configuration/hoes.yml", true, false),
                FileLocation.of("configuration/items.yml", true, false)
        )));
        this.fileManager.loadFiles();

        // Utils
        itemUtil = new ItemUtil();


        yaml = new Yaml(this);
        sql = new SQL(this);
        mySQL = new MySQL(this);

        // NBT MANAGING
        legacy = new Legacy(this, messagesHandler);
        current = new Current(this, messagesHandler);

        dataManager = new DataManager(mySQL, this, sql, yaml);
        nbtDataUtil = new NBTDataUtil(this, legacy, current); // NBT manager
        cropLeveling = new CropLeveling(this);




        cropDrops = new CropDrops(this, nbtDataUtil, cropLeveling, itemUtil);// Manage crops.
        multiCropHandler = new MultiCropHandler(this);

        // Gui handler
        handler = new Handler(this, messagesHandler, itemUtil, cropDrops);

        dataManager.openConnection(); // Open DataBase Connection
        dataManager.createTable(); // Create table if not exists

        PluginManager pm = VirtusCore.plugins();
        // Event variables

        pm.registerEvents(new CropBreakEvent(this, cropDrops, itemUtil, dataManager, messagesData, messagesHandler, multiCropHandler), this);
        pm.registerEvents(new CropPlaceEvent(this, dataManager, cropDrops, messagesData, messagesHandler, multiCropHandler, nbtDataUtil), this);
        pm.registerEvents(new CropPistonExtendEvent(this, dataManager, cropDrops, messagesData, messagesHandler, multiCropHandler), this);
        pm.registerEvents(new CropPistonRetractEvent(this, dataManager, cropDrops, messagesData, messagesHandler, multiCropHandler), this);
        pm.registerEvents(new CropGrowEvent(this, dataManager, cropDrops, messagesData, messagesHandler, multiCropHandler), this);
        pm.registerEvents(new CropInteractEvent(this, dataManager, cropDrops, messagesData, messagesHandler, multiCropHandler, nbtDataUtil, cropLeveling), this);
        pm.registerEvents(new CropFromToEvent(this, dataManager, cropDrops, messagesData, messagesHandler, multiCropHandler), this);
        pm.registerEvents(new DispenserCropInteractEvent(this, dataManager, cropDrops, messagesData, messagesHandler, multiCropHandler, nbtDataUtil), this);

        // GUI STUFF
        pm.registerEvents(new OnClickEvent(), this);
        pm.registerEvents(new CloseInventoryEvent(this, handler), this);
        pm.registerEvents(new InventoryOpenEvent(handler), this);


        //register commands
        registerCommands();


        //API
        this.cropsApi = new CropsApi(this, dataManager, cropDrops);

        console.sendMessage(TextUtil.colorFormat(line));
        // Metrics
        if (this.getConfig().getBoolean("use-metrics")) {
            MetricsLite metricsLite = new MetricsLite(this);
        }
    }

    @Override
    public void onDisable() {
        dataManager.closeConnection();
    }

    private void setupConfig(){
        messagesData.setup();
        this.saveDefaultConfig();
    }

    public FileManager getFileManager(){
        return this.fileManager;
    }

    public void reload(){
        dataManager.closeConnection();
        messagesData.reloadMessages();
        dataManager.openConnection();

        fileManager.clear();
        fileManager.loadFiles();

        this.reloadConfig();
    }

    /*public String getServerVersion(){
        return version;
    }*/


    private void registerCommands() {

        /*
            REDO ALL COMMANDS.
         */


        CommandHandler handler = new CommandHandler(messagesData);

        //Registers the command /example which has no arguments.
        handler.register("simplecrops", new MainCommand(this, this.handler, messagesData));

        //Registers the command /example args based on args[0] (args)
        handler.register("give", new GiveCommand(cropDrops, nbtDataUtil, this, messagesData, itemUtil));
        handler.register("reload", new ReloadCommand(this, messagesData));
        handler.register("clean", new CleanCommand(this, messagesData, dataManager));
        handler.register("editor", new EditorCommand(this.handler, messagesData));
        handler.register("givehoe", new GiveHoeCommand(this, nbtDataUtil, messagesData, messagesHandler));

        getCommand("simplecrops").setExecutor(handler);
        getCommand("simplecrops").setTabCompleter(new TabComplete(this));
    }
}
