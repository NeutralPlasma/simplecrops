package com.neutralplasma.simplecrops.datamanagers.storagetypes;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.utils.Crop;
import com.neutralplasma.simplecrops.utils.CropLocation;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.nio.CharBuffer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class SQL {


    private HikariDataSource hikari;
    private SimpleCrops simpleCrops;


    private Map<String, Crop> crops = new HashMap<>();
    private BukkitTask bukkitTask;
    private String tableName;
    private boolean isSyncing = false;

    public SQL(SimpleCrops simpleCrops){
        this.simpleCrops = simpleCrops;
        tableName = simpleCrops.getConfig().getString("database.tablename");
    }

    public void openConnection(){
        String database = simpleCrops.getConfig().getString("database.name");
        File file = new File(simpleCrops.getDataFolder().getAbsolutePath() + "/" + database + ".db");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        }catch (Exception error){

        }

        HikariConfig config = new HikariConfig();
        config.setPoolName("Crops");
        config.setDriverClassName("org.sqlite.JDBC");
        config.setJdbcUrl("jdbc:sqlite:" + simpleCrops.getDataFolder().getAbsolutePath() + "/" + database + ".db");
        config.setConnectionTestQuery("SELECT 1");
        config.setMaxLifetime(60000); // 60 Sec
        config.setMaximumPoolSize(10); // 50 Connections (including idle connections)

        hikari = new HikariDataSource(config);
    }
    /*public void closeConnection(){
        hikari.close();
    }

    public boolean cleanUser(String tablename, String uuid) throws SQLException{
        try(Connection connection = hikari.getConnection()){
            String statement = "DELETE FROM " + tablename + " WHERE placedBy = ?";
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                preparedStatement.setString(1, uuid);
                preparedStatement.execute();
                return true;
            }
        }
    }

    public void createTable(String tablename) throws SQLException {
        try(Connection connection = hikari.getConnection()){
            String statement = "CREATE TABLE IF NOT EXISTS " + tablename + "(" +
                    "name TEXT," +
                    "gain INT," +
                    "strength INT," +
                    "placedBy TEXT," +
                    "id TEXT," +
                    "x LONG," +
                    "y LONG," +
                    "z LONG," +
                    "world TEXT," +
                    "bonemeal INT DEFAULT 0)";
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                preparedStatement.execute();
            }
        }
    }


        public void update(String tablename) throws SQLException{
            try(Connection connection = hikari.getConnection()){
                String statement = "ALTER TABLE " + tablename + " ADD COLUMN bonemeal INT DEFAULT 0";
                try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                    preparedStatement.execute();
                }
            }
    }


    public boolean addPlant(String tablename, Crop crop, CropLocation cropLocation) throws SQLException{
        if(getPlant(tablename, cropLocation) != null){
            return false;
        }
        try(Connection connection = hikari.getConnection()){
            String statement = "INSERT INTO" +
                    "  " + tablename + " (" +
                    "    name," +
                    "    gain," +
                    "    strength," +
                    "    placedBy," +
                    "    id," +
                    "    x," +
                    "    y," +
                    "    z," +
                    "    world," +
                    "    bonemeal" +
                    "  )" +
                    "VALUES" +
                    "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                preparedStatement.setString(1, crop.getName());
                preparedStatement.setInt(2, crop.getGain());
                preparedStatement.setInt(3, crop.getStrength());
                preparedStatement.setString(4, crop.getPlacedBy().toString());
                preparedStatement.setString(5, crop.getid());
                preparedStatement.setLong(6, cropLocation.getX());
                preparedStatement.setLong(7, cropLocation.getY());
                preparedStatement.setLong(8, cropLocation.getZ());
                preparedStatement.setString(9, cropLocation.getWorld());
                preparedStatement.setInt(10, crop.getBonemeal());
                preparedStatement.execute();
                return true;
            }
        }
    }

    public Crop getPlant(String tablename, CropLocation cropLocation) throws SQLException{
        try(Connection connection = hikari.getConnection()) {
            String statement = "SELECT * FROM " + tablename + " WHERE x = ? AND z = ? AND y = ? AND world = ?";
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                preparedStatement.setLong(1, cropLocation.getX());
                preparedStatement.setLong(2, cropLocation.getZ());
                preparedStatement.setLong(3, cropLocation.getY());
                preparedStatement.setString(4, cropLocation.getWorld());
                ResultSet resultSet = preparedStatement.executeQuery();
                while(resultSet.next()){
                    UUID uuid = (UUID.fromString(resultSet.getString("placedBy")));
                    Crop crop = new Crop(resultSet.getString("name"),
                            resultSet.getInt("gain"),
                            resultSet.getInt("strength"),
                            uuid,
                            resultSet.getString("id"));
                    crop.setBonemeal(resultSet.getInt("bonemeal"));
                    return crop;
                }
            }
        }
        return null;
    }


    public boolean removePlant(String tablename, CropLocation location) throws SQLException {
        if(getPlant(tablename, location) != null) {
            try (Connection connection = hikari.getConnection()) {
                String statement = "DELETE FROM " + tablename + " WHERE x = ? AND z = ? AND y = ? AND world = ?";
                try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
                    preparedStatement.setLong(1, location.getX());
                    preparedStatement.setLong(2, location.getZ());
                    preparedStatement.setLong(3, location.getY());
                    preparedStatement.setString(4, location.getWorld());
                    preparedStatement.execute();
                }

            }
        }

        return false;
    }*/

    public void closeConnection() throws SQLException {
        bukkitTask.cancel();
        syncCrops();
        crops.clear();
        hikari.close();
        isSyncing = false;
    }

    public void createTable() throws SQLException{
        try(Connection connection = hikari.getConnection()){
            String statement = "CREATE TABLE IF NOT EXISTS " + tableName + " (" +
                    "gain INT," +
                    "strength INT," +
                    "placedBy VARCHAR," +
                    "id TEXT," +
                    "x FLOAT," +
                    "y FLOAT," +
                    "z FLOAT," +
                    "world VARCHAR," +
                    "bonemeal INT)";
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                preparedStatement.execute();
            }
        }
    }

    public boolean removePlant(CropLocation location, Crop crop){
        if(isSyncing){
            return false;
        }
        String croplocationData = location.getWorld() + ":" + location.getX() + ":" + location.getY() + ":" +
                location.getZ();
        crops.remove(croplocationData);
        return true;
    }
    public boolean addPlant(Crop crop, CropLocation location){
        if(isSyncing){
            return false;
        }
        String croplocationData = location.getWorld() + ":" + location.getX() + ":" + location.getY() + ":" +
                location.getZ();

        crops.put(croplocationData, crop);
        return true;
    }
    public Crop getPlant(CropLocation location){
        String croplocationData = location.getWorld() + ":" + location.getX() + ":" + location.getY() + ":" +
                location.getZ();
        return crops.get(croplocationData);
    }

    public void cacheCrops() throws SQLException{
        try(Connection connection = hikari.getConnection()){
            String statement = "SELECT * from " + tableName + ";";
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                int amount = 0;
                ResultSet resultSet = preparedStatement.executeQuery();
                while(resultSet.next()){
                    amount++;
                    UUID uuid = (UUID.fromString(resultSet.getString("placedBy")));
                    String id = resultSet.getString("id");
                    Crop crop = new Crop(simpleCrops.getConfig().getString("seeds." + id + ".name"),
                            resultSet.getInt("gain"),
                            resultSet.getInt("strength"),
                            uuid,
                            id);
                    crop.setBonemeal(resultSet.getInt("bonemeal"));
                    String croplocationS = resultSet.getString("world") + ":" + resultSet.getInt("x") + ":" + resultSet.getInt("y") + ":" +
                            resultSet.getInt("z");
                    crops.put(croplocationS, crop);
                }
                Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&8[&6SC&8] &7Loaded &b" + amount + " &7crops from database!"));
            }
        }
    }

    public void syncCrops() throws SQLException{
        try(Connection connection = hikari.getConnection()){
            // copy table for less lag. (smaller temporary block time)
            isSyncing = true;
            Map<String, Crop> temporary = new HashMap<>(crops);
            isSyncing = false;
            
            String statement = "DELETE FROM " +tableName + ";";
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                preparedStatement.execute();
            }
            for(String croploc : temporary.keySet()){
                Crop add = crops.get(croploc);
                String[] splited = croploc.split(":");
                String statement2 = "INSERT INTO " + tableName + "(" +
                        "gain," +
                        "strength," +
                        "placedBy," +
                        "id," +
                        "x," +
                        "y," +
                        "z," +
                        "world," +
                        "bonemeal) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                try(PreparedStatement preparedStatement = connection.prepareStatement(statement2)){
                    preparedStatement.setInt(1, add.getGain());
                    preparedStatement.setInt(2, add.getStrength());
                    preparedStatement.setString(3, add.getPlacedBy().toString());
                    preparedStatement.setString(4, add.getid());
                    preparedStatement.setLong(5, Long.valueOf(splited[1]));
                    preparedStatement.setLong(6, Long.valueOf(splited[2]));
                    preparedStatement.setLong(7, Long.valueOf(splited[3]));
                    preparedStatement.setString(8, splited[0]);
                    preparedStatement.setInt(9, add.getBonemeal());
                    preparedStatement.execute();
                }
            }
            isSyncing = false;
        }
    }
    public boolean cleanUser(String user){
        if(isSyncing){
            return false;
        }
        for(String croploc : crops.keySet()){
            Crop crop = crops.get(croploc);
            if(crop.getPlacedBy().toString().equals(user)){
                crops.remove(croploc);
            }
        }
        return true;
    }
    public void setupUpdater(){
        long updaterTime = simpleCrops.getConfig().getLong("database.save-inverval");
        bukkitTask = new BukkitRunnable(){
            @Override
            public void run() {
                try {
                    syncCrops();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskTimerAsynchronously(simpleCrops, 0L, updaterTime);
    }

    public void setup() throws SQLException{
        openConnection();
        createTable();
        setupUpdater();
        cacheCrops();
    }
}
