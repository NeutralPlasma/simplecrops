package com.neutralplasma.simplecrops.datamanagers.storagetypes;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.utils.Crop;
import com.neutralplasma.simplecrops.utils.CropLocation;
import com.zaxxer.hikari.HikariDataSource;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class MySQL {

    private HikariDataSource hikari;
    private SimpleCrops simpleCrops;

    private Map<String, Crop> crops = new HashMap<>();
    private List<String> toRemove = new ArrayList<>();
    private BukkitTask bukkitTask;
    private boolean doRemove = true;

    public MySQL(SimpleCrops simpleCrops){
        this.simpleCrops = simpleCrops;
    }

    public void openConnection(){
        String address = simpleCrops.getConfig().getString("database.address");
        String username = simpleCrops.getConfig().getString("database.user");
        String database = simpleCrops.getConfig().getString("database.name");
        String userPassword = simpleCrops.getConfig().getString("database.password");
        long timeout = simpleCrops.getConfig().getInt("database.timeout");
        int poolsize = simpleCrops.getConfig().getInt("database.poolsize");

        String[] adresses = address.split(":");

        hikari = new HikariDataSource();
        hikari.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        hikari.setMaximumPoolSize(poolsize);
        hikari.setConnectionTimeout(timeout);
        hikari.addDataSourceProperty("serverName", adresses[0]);
        hikari.addDataSourceProperty("port", adresses[1]);
        hikari.addDataSourceProperty("databaseName", database);
        hikari.addDataSourceProperty("user", username);
        hikari.addDataSourceProperty("password", userPassword);
        hikari.addDataSourceProperty("useSSL", false);

    }

    public void closeConnection(){
        String database = simpleCrops.getConfig().getString("database.tablename");
        bukkitTask.cancel();
        updateCrops(database);
        crops.clear();
        hikari.close();
        toRemove.clear();
        doRemove = true;
    }

    public void createTable(String tablename) throws SQLException {
        try(Connection connection = hikari.getConnection()){
            String statement = "CREATE TABLE IF NOT EXISTS " + tablename + "(" +
                    "name TEXT," +
                    "gain INT," +
                    "strength INT," +
                    "placedBy TEXT," +
                    "id TEXT," +
                    "x LONG," +
                    "y LONG," +
                    "z LONG," +
                    "world TEXT," +
                    "bonemeal INT DEFAULT 0)";
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                preparedStatement.execute();
            }
        }
    }

    public void update(String tablename) throws SQLException{
        try(Connection connection = hikari.getConnection()){
            String statement = "ALTER TABLE " + tablename + " ADD COLUMN bonemeal INT DEFAULT 0";
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                preparedStatement.execute();
            }
        }
    }

    public boolean cleanUser(String tablename, String uuid) throws SQLException{
        Map<String, Crop> newHashMap = new HashMap<>();
        try {
            for (String cropLocation : crops.keySet()) {
                Crop crop = crops.get(cropLocation);
                if (!crop.getPlacedBy().toString().equalsIgnoreCase(uuid)) {
                    newHashMap.put(cropLocation, crop);
                }
            }
            crops = newHashMap;
        }catch (NullPointerException error){
            return false;
        }
        return true;
    }


    public boolean addPlant(String tablename, Crop crop, CropLocation cropLocation){
        String croplocationS = cropLocation.getWorld() + ":" + cropLocation.getX() + ":" + cropLocation.getY() + ":" +
                cropLocation.getZ();
        crops.put(croplocationS, crop);
        return true;
    }

    public Crop getPlant(String tablename, CropLocation cropLocation){
        String croplocationS = cropLocation.getWorld() + ":" + cropLocation.getX() + ":" + cropLocation.getY() + ":" +
                cropLocation.getZ();
        Crop crop = crops.get(croplocationS);
        if(crop != null){
            return crop;
        }else{
            return null;
        }
    }


    public boolean removePlant(String tablename, CropLocation location){
        String croplocation = location.getWorld() + ":" + location.getX() + ":" + location.getY() + ":" +
                location.getZ();
        if(doRemove) {
            crops.remove(croplocation);
        }else{
            toRemove.add(croplocation);
        }
        return true;
    }

    public void cacheCrops(String tablename) throws SQLException{
        try(Connection connection = hikari.getConnection()) {
            String statement = "SELECT * FROM " + tablename;
            try(PreparedStatement preparedStatement = connection.prepareStatement(statement)){
                ResultSet resultSet = preparedStatement.executeQuery();
                while(resultSet.next()){
                    UUID uuid = (UUID.fromString(resultSet.getString("placedBy")));
                    Crop crop = new Crop(resultSet.getString("name"),
                            resultSet.getInt("gain"),
                            resultSet.getInt("strength"),
                            uuid,
                            resultSet.getString("id"));
                    crop.setBonemeal(resultSet.getInt("bonemeal"));
                    String croplocationS = resultSet.getString("world") + ":" + resultSet.getInt("x") + ":" + resultSet.getInt("y") + ":" +
                            resultSet.getInt("z");
                    crops.put(croplocationS, crop);
                }
            }
        }
    }

    public void setupUpdater(String tablename){
        long updaterTime = simpleCrops.getConfig().getLong("database.save-inverval");
        bukkitTask = new BukkitRunnable(){
            @Override
            public void run() {
                updateCrops(tablename);
            }
        }.runTaskTimerAsynchronously(simpleCrops, 0L, updaterTime);
    }
    public void updateCrops(String tablename){
        try(Connection connection = hikari.getConnection()) {
            String clearStatement = "TRUNCATE TABLE " + tablename;
            try (PreparedStatement preparedStatement = connection.prepareStatement(clearStatement)) {
                preparedStatement.execute();

            } catch (SQLException errors) {
                Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&cError: " + errors));
            }
            doRemove = false;
            for (String cropLocation : crops.keySet()){
                String[] truceLocation = cropLocation.split(":");
                Crop crop = crops.get(cropLocation);
                String statement = "INSERT INTO" +
                        "  " + tablename + " (" +
                        "    name," +
                        "    gain," +
                        "    strength," +
                        "    placedBy," +
                        "    id," +
                        "    x," +
                        "    y," +
                        "    z," +
                        "    world," +
                        "    bonemeal" +
                        "  )" +
                        "VALUES" +
                        "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
                    preparedStatement.setString(1, crop.getName());
                    preparedStatement.setInt(2, crop.getGain());
                    preparedStatement.setInt(3, crop.getStrength());
                    preparedStatement.setString(4, crop.getPlacedBy().toString());
                    preparedStatement.setString(5, crop.getid());
                    preparedStatement.setLong(6, Long.parseLong(truceLocation[1]));
                    preparedStatement.setLong(7, Long.parseLong(truceLocation[2]));
                    preparedStatement.setLong(8, Long.parseLong(truceLocation[3]));
                    preparedStatement.setString(9, truceLocation[0]);
                    preparedStatement.setInt(10, crop.getBonemeal());
                    preparedStatement.execute();
                } catch (SQLException errorSQL) {
                    Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&cError: " + errorSQL));
                }
            }
            doRemove = true;
            for(String cropLocation : toRemove){
                String[] truceLocation = cropLocation.split(":");
                Crop crop = crops.get(cropLocation);
                if(crop != null) {
                    String statement = "INSERT INTO" +
                            "  " + tablename + " (" +
                            "    name," +
                            "    gain," +
                            "    strength," +
                            "    placedBy," +
                            "    id," +
                            "    x," +
                            "    y," +
                            "    z," +
                            "    world" +
                            "  )" +
                            "VALUES" +
                            "  (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
                        preparedStatement.setString(1, crop.getName());
                        preparedStatement.setInt(2, crop.getGain());
                        preparedStatement.setInt(3, crop.getStrength());
                        preparedStatement.setString(4, crop.getPlacedBy().toString());
                        preparedStatement.setString(5, crop.getid());
                        preparedStatement.setLong(6, Long.parseLong(truceLocation[1]));
                        preparedStatement.setLong(7, Long.parseLong(truceLocation[2]));
                        preparedStatement.setLong(8, Long.parseLong(truceLocation[3]));
                        preparedStatement.setString(9, truceLocation[0]);
                        preparedStatement.execute();
                    } catch (SQLException errorSQL) {
                        Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&cError: " + errorSQL));
                    }
                }
            }
            toRemove.clear();
        } catch (
                SQLException error) {
            Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&cError: " + error));
        }
    }

    public void startup(String tablename){
        this.openConnection();
        try {
            cacheCrops(tablename);
        }catch (SQLException error){
            Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&cError: " + error));
        }
        this.setupUpdater(tablename);
        toRemove.clear();
        doRemove = true;

    }

}
