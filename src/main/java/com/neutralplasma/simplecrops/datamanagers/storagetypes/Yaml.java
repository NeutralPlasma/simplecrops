package com.neutralplasma.simplecrops.datamanagers.storagetypes;

import com.google.gson.Gson;
import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.utils.Crop;
import com.neutralplasma.simplecrops.utils.CropLocation;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class Yaml {


    private FileConfiguration messagesconfiguration;
    private File messagesFile;
    private SimpleCrops plugin;
    private Gson gson;

    private BukkitTask saveInterval;

    public Yaml(SimpleCrops simpleCrops){
        this.plugin = simpleCrops;
        this.gson = new Gson();
    }


    public void setup(){

        //creates plugin folder
        if(!plugin.getDataFolder().exists()){
            plugin.getDataFolder().mkdir();
        }
        //---------------------

        messagesFile = new File(plugin.getDataFolder(), "data.yml");
        if(!messagesFile.exists()){
            try{
                messagesFile.createNewFile();
                messagesconfiguration = YamlConfiguration.loadConfiguration(messagesFile);
                plugin.saveResource("data.yml", true);
                Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&aSuccessfully created data.yml file!"));


            }catch (IOException e){
                Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&cFailed to create data.yml file, Error: &f" + e.getMessage()));

            }

        }
        messagesconfiguration = YamlConfiguration.loadConfiguration(messagesFile);
        saveUpdate(plugin.getConfig().getLong("database.yaml-save-interval"));
    }

    public FileConfiguration getData() {
        return messagesconfiguration;
    }


    public void saveData(){
        try{
            messagesconfiguration.save(messagesFile);
            //Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&aSuccessfully saved data.yml file."));
        }catch(IOException e){
            Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&cFailed to save data.yml file, Error: &f" + e.getMessage()));
        }
    }

    public void reloadData() {
        messagesconfiguration = YamlConfiguration.loadConfiguration(messagesFile);
        saveInterval.cancel();
        Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&aReloaded data.yml file."));
    }

    public void stopUpdate(){
        saveInterval.cancel();
    }


    public boolean addPlant(Crop crop, CropLocation cropLocation){
        try {
            String player = crop.getPlacedBy().toString();
            String cropString = gson.toJson(crop);
            String cropLocationString = gson.toJson(cropLocation);
            getData().set("crops." + cropLocationString+ ".crop",cropString);
            getData().set("crops." + cropLocationString + ".player", player);
            return true;
        }catch (Exception error){
            return false;
        }
    }

    public Crop getPlant(CropLocation cropLocation){
        try{
            String cropLocationString = gson.toJson(cropLocation);
            String cropString = getData().getString("crops." + cropLocationString + ".crop");
            Crop crop = gson.fromJson(cropString, Crop.class);
            return crop;
        }catch (Exception error){
            return null;
        }
    }

    public boolean removeCrop(CropLocation cropLocation){
        try {
            String cropLocationString = gson.toJson(cropLocation);
            getData().set("crops." + cropLocationString, null);
            return true;
        }catch (Exception error){
            return false;
        }
    }

    public boolean cleanUser(String uuid){
        try {
            ConfigurationSection configurationSection = getData().getConfigurationSection("crops");
            for (String string : configurationSection.getKeys(true)) {
                if (string.contains(".player")) {
                    Bukkit.getConsoleSender().sendMessage("Player string: " + string);
                    String player;
                    player = getData().getString("crops." + string);
                    if (player == null) {
                        player = "";
                    }
                    if (player.equalsIgnoreCase(uuid)) {
                        String location = string.replace(".player", "");
                        CropLocation clocation = gson.fromJson(location, CropLocation.class);
                        removeCrop(clocation);
                    }
                }
            }
            saveData();
            return true;
        }catch (Exception error){
            return false;
        }
    }

    public HashMap<CropLocation, Crop> getCrops(String player){
        HashMap<CropLocation, Crop> crops = new HashMap<>();
        try{
            ConfigurationSection configurationSection = getData().getConfigurationSection("crops");
            if (configurationSection == null) {
                return crops;
            }
            for(String string : configurationSection.getKeys(true)){
                String location = string.replace(".player", "");
                String crop = getData().getString(location + ".crop");
                String playerUUID = getData().getString(location + ".player");
                CropLocation clocation = gson.fromJson(location, CropLocation.class);
                Crop cropl = gson.fromJson(crop, Crop.class);
                if(player.equalsIgnoreCase("*")){
                    crops.put(clocation, cropl);
                }else if(player.equalsIgnoreCase(playerUUID)){
                    crops.put(clocation, cropl);
                }
            }
        }catch (Exception error){
            Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&cSomething went wrong while getting crops. " + error));
        }
        return crops;
    }

    public void saveUpdate(long interval){
        saveInterval = new BukkitRunnable() {
            @Override
            public void run() {
                saveData();
            }
        }.runTaskTimer(plugin, 0L, interval);
    }
}
