package com.neutralplasma.simplecrops.datamanagers;

import com.neutralplasma.simplecrops.SimpleCrops;
import com.neutralplasma.simplecrops.datamanagers.storagetypes.MySQL;
import com.neutralplasma.simplecrops.datamanagers.storagetypes.SQL;
import com.neutralplasma.simplecrops.datamanagers.storagetypes.Yaml;
import com.neutralplasma.simplecrops.utils.Crop;
import com.neutralplasma.simplecrops.utils.CropLocation;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.SQLException;

public class DataManager {

    private MySQL mySQL;
    private SQL sql;
    private SimpleCrops simpleCrops;
    private Yaml yaml;
    private String tablename;
    private String engine;

    public DataManager(MySQL mySQL, SimpleCrops simpleCrops, SQL sql, Yaml yaml){
        this.mySQL = mySQL;
        this.sql = sql;
        this.simpleCrops = simpleCrops;
        this.yaml = yaml;

        engine = simpleCrops.getConfig().getString("database-engine");
        tablename = simpleCrops.getConfig().getString("database.tablename");
    }

    public void openConnection(){
        if(engine.equalsIgnoreCase("sql")){
            try {
                sql.setup();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(engine.equalsIgnoreCase("mysql")) {
            mySQL.startup(tablename);
        }
        if(engine.equalsIgnoreCase("yaml")){
            yaml.setup();
        }
    }
    public void closeConnection(){

        if(engine.equalsIgnoreCase("sql")){
            try {
                sql.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(engine.equalsIgnoreCase("mysql")) {
            mySQL.closeConnection();
        }
        if(engine.equalsIgnoreCase("yaml")){
            yaml.stopUpdate();
            yaml.saveData();
        }
    }

    public void createTable(){
        try {
            if(engine.equalsIgnoreCase("sql")){
                sql.createTable();
            }
            if(engine.equalsIgnoreCase("mysql")) {
                mySQL.createTable(tablename);
            }
        }catch (SQLException error) {
            Bukkit.getConsoleSender().sendMessage("Error occured while creating table: " + error.getMessage());
        }
        try{
            if(engine.equalsIgnoreCase("sql")){
                if(!simpleCrops.getConfig().getBoolean("database.updated")){
                    //sql.update(tablename);
                    Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&8[&eSimpleCrops&8] &7Updated database."));
                    simpleCrops.getConfig().set("database.updated", true);
                    simpleCrops.saveConfig();
                }
            }
            if(engine.equalsIgnoreCase("mysql")) {
                if(!simpleCrops.getConfig().getBoolean("database.updated")){
                    mySQL.update(tablename);
                    simpleCrops.getConfig().set("database.updated", true);
                    Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&8[&eSimpleCrops&8] &7Updated database."));
                    simpleCrops.saveConfig();
                }
            }
        }catch (SQLException error) {
            if(error.toString().contains("duplicate")) {
                simpleCrops.getConfig().set("database.updated", true);
                Bukkit.getConsoleSender().sendMessage(TextUtil.colorFormat("&8[&eSimpleCrops&8] &7Updated database."));
                simpleCrops.saveConfig();
            }else{
                Bukkit.getConsoleSender().sendMessage("Error occured while updating table: " + error.getMessage());
            }
        }
    }

    public boolean addPlant(Crop crop, CropLocation cropLocation){
        if(engine.equalsIgnoreCase("sql")){
            return sql.addPlant(crop, cropLocation);
        }
        if(engine.equalsIgnoreCase("mysql")) {
            return mySQL.addPlant(tablename, crop, cropLocation);
        }
        if(engine.equalsIgnoreCase("yaml")){
            return yaml.addPlant(crop, cropLocation);
        }
        return false;
    }

    public Crop getCrop(CropLocation cropLocation){
        if(engine.equalsIgnoreCase("sql")){
            return sql.getPlant(cropLocation);
        }
        if(engine.equalsIgnoreCase("mysql")) {
            return mySQL.getPlant(tablename, cropLocation);
        }
        if(engine.equalsIgnoreCase("yaml")){
            return yaml.getPlant(cropLocation);
        }
        return null;
    }

    public boolean removeCrop(CropLocation location, Crop crop){
        if(engine.equalsIgnoreCase("sql")){
            if(sql.removePlant(location, crop)){
                return true;
            }
        }
        if(engine.equalsIgnoreCase("mysql")) {

            return mySQL.removePlant(tablename, location);
        }
        if(engine.equalsIgnoreCase("yaml")){
            return yaml.removeCrop(location);
        }
        return false;

    }

    public boolean cleanPlayer(String uuid){
        try{
            if(engine.equalsIgnoreCase("sql")){
                if(sql.cleanUser(uuid)){
                    return true;
                }
            }
            if(engine.equalsIgnoreCase("mysql")){
                return mySQL.cleanUser(tablename, uuid);
            }
            if(engine.equalsIgnoreCase("yaml")){
                return yaml.cleanUser(uuid);
            }
        }catch (SQLException error){
            Bukkit.getConsoleSender().sendMessage("Error occured while cleaning user " + uuid + " :" + error.getMessage());
            return false;
        }
        return false;
    }
}
